#pragma once

#include <algorithm>
#include <iostream>
#include <random>
#include <string>

#include <pcg_random.hpp>

extern "C" {
#include <SDL.h>
}

#include "base.h"
#include "filmic.h"

using std::clamp;
using std::max, std::min;
using std::ostream;
using std::string;

extern int WINDOW_WIDTH;
extern int WINDOW_HEIGHT;

extern unsigned int RENDER_DONE;
extern bool exit_program;

inline uint8_t to8bit(real n) {
    //real nfilmic = filmic_tone_map(n); //XXX broken
    real nfilmic = pw_tone_map(n); //XXX use piecewise linear fake instead
    real ngamma = pow(nfilmic, 1/2.2);
    uint8_t n8bit = uint8_t(255 * ngamma + 0.5);
    return n8bit;
}

struct color_t {
    real red;
    real green;
    real blue;

    inline uint8_t red8() {return isnan(red) ? 255 : to8bit(red);}
    inline uint8_t green8() {return isnan(green) ? 0 : to8bit(green);}
    inline uint8_t blue8() {return isnan(blue) ? 255 : to8bit(blue);}

    inline bool no_light() {return red == 0 && green == 0 && blue == 0;}

    inline color_t operator+(color_t c) {
        return {red + c.red, green + c.green, blue + c.blue};
    }
    inline color_t operator+=(color_t c) {
        return {red += c.red, green += c.green, blue += c.blue};
    }
    inline color_t operator-(color_t c) {
        return {red - c.red, green - c.green, blue - c.blue};
    }
    inline color_t operator*(color_t c) {
        return {red * c.red, green * c.green, blue * c.blue};
    }
    inline color_t operator*=(color_t c) {
        return {red *= c.red, green *= c.green, blue *= c.blue};
    }
    inline color_t operator*=(real k) {
        return {red *= k, green *= k, blue *= k};
    }
    inline color_t operator/=(real k) {
        return {red /= k, green /= k, blue /= k};
    }
    inline color_t operator*(real k) {return {red*k, green*k, blue*k};}
    inline color_t operator/(real k) {return {red/k, green/k, blue/k};}

    inline bool small() {
        return red < 1/256. && green < 1/256. && blue < 1/256.;
    }
};

inline color_t operator*(real k, color_t c) {
    return {c.red*k, c.green*k, c.blue*k};
}

ostream & operator<<(ostream & out, color_t c);

namespace std {
string to_string(color_t c);
}

inline real max_of(color_t c) {
    return max(max(c.red, c.green), c.blue);
}

inline real min_of(color_t c) {
    return min(min(c.red, c.green), c.blue);
}

void die(string message) __attribute__((noreturn));
void warn(string message);
void info(string message);

extern thread_local pcg32 gen;
extern std::uniform_real_distribution<real> r01;
extern std::uniform_real_distribution<real> r55;
extern std::uniform_real_distribution<real> r11;
extern std::uniform_real_distribution<real> r0tau;

template<typename T>
inline T sq(T n) {
    return n*n;
}

template<typename T>
inline T cb(T n) {
    return n * sq(n);
}

template<typename T>
inline T four(T n) {
    return sq(sq(n));
}

template<typename T>
inline T six(T n) {
    return four(n) * sq(n);
}

extern SDL_Window * window;
extern SDL_Surface * surface;
