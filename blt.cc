#include <iostream>
#include <random>
#include <thread>

#include <lyra/lyra.hpp>

extern "C" {
#include <SDL.h>
#include <SDL_image.h>
}

#include "blt.h"
#include "render.h"
#include "world.h"
#include "camera.h"
#include "scene.h"

using std::cout, std::cerr, std::endl;
using std::string;
using std::thread;

int WINDOW_WIDTH = 1000;
int WINDOW_HEIGHT = 1000;

ostream & operator<<(ostream & out, color_t c) {
    out << c.red << "," << c.green << "," << c.blue;
    return out;
}

namespace std {
string to_string(color_t c) {
    ostringstream s;
    s << c;
    return s.str();
}
}

void die(string message) {
    cerr << message << endl;
    exit(1);
}

void warn(string message) {
    cerr << message << endl;
}

void info(string message) {
    cerr << message << endl;
}

thread_local pcg_extras::seed_seq_from<std::random_device> seed_source;
thread_local pcg32 gen(seed_source);
std::uniform_real_distribution<real> r01(0,1);
std::uniform_real_distribution<real> r55(-0.5,0.5);
std::uniform_real_distribution<real> r11(-1,1);
std::uniform_real_distribution<real> r0tau(0,2*M_PI);

char WINDOW_NAME[] = "Basic Light Transport";
char WINDOW_NAME_DONE[] = "Render done! Basic Light Transport";
SDL_Window * window;
SDL_Surface * surface;

void init()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) die("SDL_INIT");

    window = SDL_CreateWindow(WINDOW_NAME,
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              WINDOW_WIDTH, WINDOW_HEIGHT, 0);
    if (window == nullptr) die("window");

    surface = SDL_GetWindowSurface(window);
    if (surface == nullptr) die("surface");
}

void close()
{
    SDL_DestroyWindow(window);
    window = nullptr;

    SDL_Quit();
}

void show_filmic() {
    cout << "filmic curve:" << endl;
    for (real x=0 ; x<5 ; x+=0.1) {
        cout << x << ": " << filmic_tone_map(x) << endl;
    }
}

void save_render(string filename) {
    if (IMG_SavePNG(surface, filename.c_str()) != 0) {
        warn("failed to save " + filename);
    }
    else warn("saved as " + filename);
}

unsigned int RENDER_DONE;
bool exit_program = false;

int main(int nargs, char * args[]) {
    bool help = false;
    int threads = thread::hardware_concurrency(); if (! threads) threads = 1;
    int samples = 16;
    string save_filename = "render.png";
    bool resume = false;
    auto cli = lyra::cli_parser()
        | lyra::help(help)
        | lyra::opt(naive)
            ["-n"]["--naive"]
            ("Naive path tracing.")
        | lyra::opt(threads, "threads")
            ["-t"]["--threads"]
            ("Number of render threads.")
        | lyra::opt(MAX_BOUNCES, "bounces")
            ["-b"]["--bounces"]
            ("Max number of bounces.")
        | lyra::opt(samples, "samples")
            ["-s"]["--samples"]
            ("Number of samples per pixel.")
        | lyra::opt(save_filename, "file")
            ["-f"]["--file"]
            ("Save file name.")
    ;
    auto ok = cli.parse({nargs, args});
    if (! ok || help) {
        cout << cli << endl;
        return bool(ok);
    }

    //XXX broken
    //setup_filmic();
    //show_filmic();

    init();

    setup_renderdata(resume);

    RENDER_DONE = SDL_RegisterEvents(1);
    thread tracethread(render_scene, threads, samples);

    bool early_exit = true;
    bool done = false;
    while (! done)
    {
        SDL_Event e;
        SDL_WaitEvent(& e); //TODO check for error

        if (e.type == SDL_QUIT) done = true;
        else if (e.type == RENDER_DONE) {
            early_exit = false;
            save_render(save_filename);
            SDL_SetWindowTitle(window, WINDOW_NAME_DONE);
        }
        else if (e.type == SDL_KEYDOWN) {
            if (e.key.keysym.sym == SDLK_ESCAPE) done = true;
        }
        else if (e.type == SDL_MOUSEBUTTONDOWN) {
            auto [x,y] = (int[]) {e.button.x, e.button.y};
            optional<hit_t> hit = scene.trace_ray(camera->pixel_ray(x, y));
            if (hit) {
                cout << "pos: " << hit->pos << endl;
                cout << "dist: " << hit->dist << endl;
            }
            else cout << "miss" << endl;

            int pix = y * WINDOW_WIDTH + x;
            pixel_t pixel = pixel_samples[pix];
            color_t color = pixel.sum / pixel.samples;
            cout << "color: " << color << endl;
        }
    }

    exit_program = true;

    if (early_exit) {
        warn("render canceled");
        save_render("canceled-" + save_filename);
    }

    tracethread.join();

    close();

    return 0;
}
