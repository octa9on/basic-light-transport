#pragma once

#include <array>
#include <functional>
#include <optional>
#include <tuple>
#include <vector>

#include "blt.h"
#include "linalg.h"

using std::array;
using std::function;
using std::optional, std::nullopt;
using std::pair;
using std::tuple;
using std::vector;

struct texture_t {
    static constexpr SDL_PixelFormatEnum IMG_FORMAT = SDL_PIXELFORMAT_ARGB8888;

    int width;
    int height;
    vector<vector<color_t>> texels;

    texture_t(string filename);

    color_t texel(real hit_u, real hit_v);
};

struct shape_hit_t;
struct shader_t {
    virtual bool emits() {return false;}
    virtual bool reflects() {return true;}
    virtual color_t emission(shape_hit_t * hit) {return {0,0,0};}
    virtual pair<color_t, real> rfl(shape_hit_t * hit, vector_t from) = 0;
    virtual tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit) = 0;
};

struct lambertian : shader_t {
    color_t color;

    lambertian() {}
    lambertian(color_t c) : color(c) {}

    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from);
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit);
};

struct lambertian_map : lambertian {
    texture_t * texture;

    lambertian_map(texture_t * t) : texture(t) {}

    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from);
};

struct perfect_mirror : shader_t {
    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from);
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit);
};

struct emissive : shader_t {
    color_t color;

    emissive(color_t c) : color(c) {}

    bool emits() {return true;}
    bool reflects() {return false;}
    color_t emission(shape_hit_t * hit) {return color;}
    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from);
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit);
};

struct add_emissive : shader_t {
    color_t color;
    shader_t * shader;

    add_emissive(color_t c, shader_t * s) : color(c), shader(s) {}

    bool emits() {return true;}
    bool reflects() {return shader->reflects();}
    color_t emission(shape_hit_t * hit) {return color;}
    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from) {
        return shader->rfl(hit, from);
    }
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit) {
        return shader->sample_rfl(hit);
    }
};

struct emissive_lambertian : lambertian {
    color_t e_color;

    emissive_lambertian(color_t c, color_t e) : lambertian(c), e_color(e) {}

    bool emits() {return true;}
    color_t emission(shape_hit_t * hit) {return e_color;}
};

struct emissive_map : shader_t {
    real scale;
    texture_t * texture;
    shader_t * shader;

    emissive_map(real c, texture_t * t, shader_t * s)
            : scale(c), texture(t), shader(s) {}

    bool emits() {return true;}
    bool reflects() {return shader->reflects();}
    color_t emission(shape_hit_t * hit);
    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from);
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit);
};

struct blank : shader_t {
    bool emits() {return false;}
    bool reflects() {return false;}
    color_t emission(shape_hit_t * hit) {return {0,0,0};}
    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from) {
        return {{0,0,0}, 0};
    }
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit) {
        return {{0,0,0},{0,0,0},0};
    }
};

struct shape_hit_t {
    real dist;
    point_t pos; // hit point in world space TODO rename to w_pos?
    point_t o_pos; // hit point in object (shape?) space
    vector_t inc;
    normal_t nml;
    real u, v;
    real pdf;
    bool exit=false; // this hit implies the ray has exited the scene

    shape_hit_t() {}
    shape_hit_t(real d, point_t p, vector_t i, normal_t n, real u, real v,
                real f)
            : dist(d), pos(p), o_pos(p), inc(i), nml(n), u(u), v(v), pdf(f) {}

    void update_to_world(transform_t & t);
};

template<typename T>
struct multishader_t : shader_t {
    real param;
    vector<shader_t *> shaders;

    multishader_t(real p, vector<shader_t *> ss)
            : param(p), shaders(ss) {}

    bool emits();
    bool reflects();
    inline int which(shape_hit_t * hit);
    color_t emission(shape_hit_t * hit);
    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from);
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit);
};

template<typename T>
bool multishader_t<T>::emits() {
    return shaders[0]->emits() || shaders[1]->emits();
}

template<typename T>
bool multishader_t<T>::reflects() {
    return shaders[0]->reflects() || shaders[1]->reflects();
}

template<typename T>
int multishader_t<T>::which(shape_hit_t * hit) {
    return T()(param, hit) % shaders.size();
}

template<typename T>
color_t multishader_t<T>::emission(shape_hit_t * hit) {
    return shaders[which(hit)]->emission(hit);
}

template<typename T>
pair<color_t, real> multishader_t<T>::rfl(shape_hit_t * hit, vector_t from) {
    return shaders[which(hit)]->rfl(hit, from);
}

template<typename T>
tuple<vector_t, color_t, real> multishader_t<T>::sample_rfl(shape_hit_t * hit) {
    return shaders[which(hit)]->sample_rfl(hit);
}

constexpr auto crosshatch_fn = [](real param, shape_hit_t * hit) -> int
{
    auto frob = [=](real n) {
        return int(abs(n) * param + 0.5);
    };

    return (frob(hit->o_pos.x) & 1) ^ (frob(hit->o_pos.y) & 1) ^ (frob(hit->o_pos.z) & 1);
};

template<typename T=decltype(crosshatch_fn)>
struct crosshatch : multishader_t<T> {
    crosshatch(real p, shader_t * s1, shader_t * s2)
            : multishader_t<T>(p, {s1, s2}) {}
};

constexpr auto blend_fn = [](real param, shape_hit_t * hit) -> int
{
    return r01(gen) > param;
};

template<typename T=decltype(blend_fn)>
struct blend : multishader_t<T> {
    blend(real p, shader_t * s1, shader_t * s2)
            : multishader_t<T>(p, {s1, s2}) {}
};

constexpr auto u_stripe_fn = [](real param, shape_hit_t * hit) -> int
{
    return abs(hit->v) >= param;
};

template<typename T=decltype(u_stripe_fn)>
struct u_stripe : multishader_t<T> {
    u_stripe(real p, shader_t * s1, shader_t * s2)
            : multishader_t<T>(p, {s1, s2}) {}
};

constexpr auto u_stripes_fn = [](real param, shape_hit_t * hit) -> int
{
    auto frob = [=](real n) {
        return int(abs(n) * param + 0.5);
    };

    return frob(hit->v) & 1;
};

template<typename T=decltype(u_stripes_fn)>
struct u_stripes : multishader_t<T> {
    u_stripes(real p, shader_t * s1, shader_t * s2)
            : multishader_t<T>(p, {s1, s2}) {}
};

constexpr auto beachball_fn = [](real param, shape_hit_t * hit) -> int
{
    if (abs(hit->v) > 0.8 || int((hit->u + 1) * param) & 1) return 0;
    return int((hit->u + 1) * param/2.) + 1;
};

template<typename T=decltype(beachball_fn)>
struct beachball : multishader_t<T> {
    beachball(vector<shader_t *> ss)
            : multishader_t<T>(ss.size()-1, ss) {}
};

struct blend_map : shader_t {
    texture_t * texture;
    shader_t * shader1;
    shader_t * shader2;

    blend_map(texture_t * t, shader_t * s1, shader_t * s2)
            : texture(t), shader1(s1), shader2(s2) {}

    pair<color_t, real> rfl(shape_hit_t * hit, vector_t from);
    tuple<vector_t, color_t, real> sample_rfl(shape_hit_t * hit);
};

struct object_t;
struct hit_t : shape_hit_t {
    object_t * object;
    shader_t * shader;

    hit_t(shape_hit_t h, object_t * o, shader_t * s)
            : shape_hit_t(h), object(o), shader(s) {}
};

struct shape_t {
    virtual optional<shape_hit_t> trace_ray(ray_t ray) = 0;
    virtual shape_hit_t sample_shape(point_t viewpoint) = 0;
    virtual real areal_pdf(shape_hit_t * hit) = 0; //XXX what does this even mean?
};

struct object_t {
    transform_t pose;
    optional<transform_t> shader_pose;
    shape_t * shape;
    shader_t * shader;

    object_t(shape_t * sp, shader_t * sd) : pose(), shader_pose(nullopt),
                                            shape(sp), shader(sd) {}

    void update_pose(transform_t t);

    inline bool emits() {return shader->emits();}
    optional<hit_t> trace_ray(ray_t ray);
    hit_t sample_object(point_t viewpoint);
};
template class vector<object_t *>; // ensure all methods available in gdb

inline object_t * obj(shape_t * sp, shader_t * sd) {
    return new object_t(sp, sd);
}

template <typename... Ts>
vector<object_t *> move(vector_t v, Ts... os) {
    auto t = transform_t::translation(v);
    vector<object_t *> ros;
    transform(t, ros, os...);
    return ros;
}

template <typename... Ts>
vector<object_t *> rot(point_t p, vector_t axis, pair<real, real> cs, Ts... os) {
    vector_t v = p - (point_t) {0,0,0};
    auto unscoot = transform_t::translation(v);
    auto rotate = transform_t::rotation(axis, cs);
    auto scoot = transform_t::translation(-v);
    auto t = unscoot * rotate * scoot;
    vector<object_t *> ros;
    transform(t, ros, os...);
    return ros;
}
template <typename... Ts>
vector<object_t *> rot(point_t p, vector_t axis, real ang, Ts... os) {
    return rot(p, axis, {cos(ang), sin(ang)}, os...);
}
template <typename... Ts>
vector<object_t *> rot(ray_t from, vector_t to, Ts... os) {
    vector_t fv = from.dir.normalize();
    vector_t tv = to.normalize();
    vector_t axis = cross(fv, tv);
    real sinang = axis.length();
    real cosang = dot(fv, tv);
    return rot(from.src, axis, {cosang, sinang}, os...);
}

template <typename... Ts>
vector<object_t *> scale(point_t p, vector_t s, Ts... os) {
    vector_t v = p - (point_t) {0,0,0};
    auto unscoot = transform_t::translation(v);
    auto scale = transform_t::scaling(s);
    auto scoot = transform_t::translation(-v);
    auto t = unscoot * scale * scoot;
    vector<object_t *> ros;
    transform(t, ros, os...);
    return ros;
}
template <typename... Ts>
inline vector<object_t *> scale(point_t p, real s, Ts... os) {
    return scale(p, {s,s,s}, os...);
}

template <typename T, typename... Ts>
void transform(transform_t & t, vector<object_t *> & ros, T o, Ts... os) {
    transform(t, ros, o);
    transform(t, ros, os...);
}
void transform(transform_t & t, vector<object_t *> & ros, object_t * o);
void transform(transform_t & t, vector<object_t *> & ros, vector<object_t *> os);

struct scene_t {
    vector<object_t *> lights;
    vector<object_t *> items;

    scene_t() = default;
    template <typename... Ts>
    scene_t(Ts... os) {add(os...);}

    template<typename T, typename... Ts>
    void add(T o, Ts... os) {
        add(o);
        add(os...);
    }
    void add(object_t * o);
    void add(vector<object_t *> os);

    optional<hit_t> trace_ray(ray_t ray);
    tuple<object_t *, color_t, vector_t, real> sample_light_at(hit_t * hit);
    bool path_clear(hit_t * from_hit, hit_t * to_hit);
};

struct skysphere_s : shape_t {
    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct triangle_s : shape_t {
    point_t p1, p2, p3;

    triangle_s(point_t p1, point_t p2, point_t p3)
            : p1(p1), p2(p2), p3(p3) {}

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct sphere_s : shape_t {
    point_t ctr;
    real r;

    sphere_s(point_t c, real r) : ctr(c), r(r) {}

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct eggthing_s : shape_t {
    sphere_s bound;

    eggthing_s();

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct thing_s : shape_t {
    sphere_s bound;

    thing_s();

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct revsolid_s : shape_t {
    function<real(real, real)> fn;
    sphere_s bound;

    revsolid_s(function<real(real, real)> f, sphere_s b) : fn(f), bound(b) {}

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct plane_s : shape_t {
    point_t pt;
    normal_t nml;

    plane_s(point_t p, normal_t n) : pt(p), nml(n) {}

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct one_sided_plane_s : shape_t {
    point_t pt;
    normal_t nml;

    one_sided_plane_s(point_t p, normal_t n) : pt(p), nml(n) {}

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct box_s : shape_t {
    point_t ctr;
    real xr, yr, zr;
    array<normal_t, 6> face_nml;
    array<point_t, 6> face_ctr;
    array<vector_t, 6> face_u;
    array<real, 6> face_ur;
    array<vector_t, 6> face_v;
    array<real, 6> face_vr;

    box_s(point_t c, real x, real y, real z);

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};

struct csg_diff : shape_t {
    shape_t * solid;
    shape_t * removed;

    csg_diff(shape_t * s, shape_t * r) : solid(s), removed(r) {}

    optional<shape_hit_t> trace_ray(ray_t ray);
    shape_hit_t sample_shape(point_t viewpoint);
    real areal_pdf(shape_hit_t * hit);
};
