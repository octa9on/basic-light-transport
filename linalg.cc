#include <sstream>

#include "linalg.h"

vector_t vector_t::rotate_by(vector_t raw_axis) {
    // axis length is number of half-rotations
    real angle = raw_axis.length() * M_PI;
    if (angle == 0) return * this;
    vector_t axis = raw_axis.normalize();

    vector_t rot = {0.0,0.0,0.0};

    real cosang = cos(angle);
    real sinang = sin(angle);

    rot.x += (cosang + (1 - cosang) * axis.x * axis.x) * x;
    rot.x += ((1 - cosang) * axis.x * axis.y - axis.z * sinang) * y;
    rot.x += ((1 - cosang) * axis.x * axis.z + axis.y * sinang) * z;

    rot.y += ((1 - cosang) * axis.x * axis.y + axis.z * sinang) * x;
    rot.y += (cosang + (1 - cosang) * axis.y * axis.y) * y;
    rot.y += ((1 - cosang) * axis.y * axis.z - axis.x * sinang) * z;

    rot.z += ((1 - cosang) * axis.x * axis.z - axis.y * sinang) * x;
    rot.z += ((1 - cosang) * axis.y * axis.z + axis.x * sinang) * y;
    rot.z += (cosang + (1 - cosang) * axis.z * axis.z) * z;

    return rot;
}

vector_t vector_t::rotate(vector_t src, vector_t dst) {
    src = src.normalize();
    dst = dst.normalize();

    real cosang = dot(src, dst);
    vector_t raw_axis = cross(src, dst);
    if (raw_axis.sq_length() == 0) {
        if (cosang > 0) return * this;
        else return (* this) * -1;
    }
    real sinang = raw_axis.length();
    vector_t axis = raw_axis / sinang;

    vector_t rot = {0.0,0.0,0.0};

    rot.x += (cosang + (1 - cosang) * axis.x * axis.x) * x;
    rot.x += ((1 - cosang) * axis.x * axis.y - axis.z * sinang) * y;
    rot.x += ((1 - cosang) * axis.x * axis.z + axis.y * sinang) * z;

    rot.y += ((1 - cosang) * axis.x * axis.y + axis.z * sinang) * x;
    rot.y += (cosang + (1 - cosang) * axis.y * axis.y) * y;
    rot.y += ((1 - cosang) * axis.y * axis.z - axis.x * sinang) * z;

    rot.z += ((1 - cosang) * axis.x * axis.z - axis.y * sinang) * x;
    rot.z += ((1 - cosang) * axis.y * axis.z + axis.x * sinang) * y;
    rot.z += (cosang + (1 - cosang) * axis.z * axis.z) * z;

    return rot;
}

vector_t vector_t::from_world(transform_t & t) {
    return t.iapply(* this);
}

vector_t vector_t::to_world(transform_t & t) {
    return t.apply(* this);
}

normal_t normal_t::from_world(transform_t & t) {
    return t.iapply(* this);
}

normal_t normal_t::to_world(transform_t & t) {
    return t.apply(* this);
}

normal_t normal_t::rotate(vector_t from, vector_t to) {
    return mknormal_t(mkvector_t(* this).rotate(from, to));
}

point_t point_t::rotate(ray_t from, vector_t to) {
    return from.src + (* this - from.src).rotate(from.dir, to);
}

point_t point_t::from_world(transform_t & t) {
    return t.iapply(* this);
}

point_t point_t::to_world(transform_t & t) {
    return t.apply(* this);
}

ray_t ray_t::from_world(transform_t & t) {
    return {src.from_world(t), dir.from_world(t)};
}

ray_t ray_t::to_world(transform_t & t) {
    return {src.to_world(t), dir.to_world(t)};
}

ostream & operator<<(ostream & out, vector_t v) {
    out << v.x << "," << v.y << "," << v.z;
    return out;
}

ostream & operator<<(ostream & out, point_t p) {
    out << p.x << "," << p.y << "," << p.z;
    return out;
}

namespace std {
string to_string(vector_t v) {
    ostringstream s;
    s << v;
    return s.str();
}
}

transform_t transform_t::translation(vector_t v) {
    transform_t t;
    t.mat[0][3] = v.x;
    t.mat[1][3] = v.y;
    t.mat[2][3] = v.z;
    t.imat[0][3] = - v.x;
    t.imat[1][3] = - v.y;
    t.imat[2][3] = - v.z;
    return t;
}

transform_t transform_t::rotation(vector_t axis, pair<real, real> cs) {
    vector_t a = axis.normalize();
    auto [cosang, sinang] = cs;

    transform_t t;
    t.mat[0][0] = t.imat[0][0] = sq(a.x) + (1 - sq(a.x)) * cosang;
    t.mat[0][1] = t.imat[1][0] = a.x * a.y * (1 - cosang) - a.z * sinang;
    t.mat[0][2] = t.imat[2][0] = a.x * a.z * (1 - cosang) + a.y * sinang;
    t.mat[1][0] = t.imat[0][1] = a.x * a.y * (1 - cosang) + a.z * sinang;
    t.mat[1][1] = t.imat[1][1] = sq(a.y) + (1 - sq(a.y)) * cosang;
    t.mat[1][2] = t.imat[2][1] = a.y * a.z * (1 - cosang) - a.x * sinang;
    t.mat[2][0] = t.imat[0][2] = a.x * a.z * (1 - cosang) - a.y * sinang;
    t.mat[2][1] = t.imat[1][2] = a.y * a.z * (1 - cosang) + a.x * sinang;
    t.mat[2][2] = t.imat[2][2] = sq(a.z) + (1 - sq(a.z)) * cosang;
    return t;
}

transform_t transform_t::scaling(vector_t s) {
    transform_t t;
    t.mat[0][0] = s.x;
    t.mat[1][1] = s.y;
    t.mat[2][2] = s.z;
    t.imat[0][0] = 1/s.x;
    t.imat[1][1] = 1/s.y;
    t.imat[2][2] = 1/s.z;
    return t;
}

vector_t transform_t::apply(vector_t v) {
    return mat * v;
}

vector_t transform_t::iapply(vector_t v) {
    return imat * v;
}

// normals transform by the transpose of the inverse
// swap mat and imat here, apply transpose in operator*
normal_t transform_t::apply(normal_t n) {
    return imat * n; // use the inverse here
}

normal_t transform_t::iapply(normal_t n) {
    return mat * n; // use the direct here
}

point_t transform_t::apply(point_t p) {
    return mat * p;
}

point_t transform_t::iapply(point_t p) {
    return imat * p;
}

transform_t transform_t::operator*(transform_t & t2) {
    transform_t & t1 = * this;
    transform_t t;
    for (int y=0 ; y<4 ; y+=1) {
        for (int x=0 ; x<4 ; x+=1) {
            real sum = 0;
            real isum = 0;
            for (int ix=0 ; ix<4 ; ix+=1) {
                sum += t1.mat[y][ix] * t2.mat[ix][x];
                isum += t2.imat[y][ix] * t1.imat[ix][x]; // swapped order
            }
            t.mat[y][x] = sum;
            t.imat[y][x] = isum;
        }
    }
    return t;
}

vector_t operator*(mat4 & m, vector_t v) {
    vector_t vr;
    vr.x = m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z;
    vr.y = m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z;
    vr.z = m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z;
    return vr;
}

// multiplies by the transpose
normal_t operator*(mat4 & m, normal_t n) {
    normal_t nr;
    nr.x = m[0][0] * n.x + m[1][0] * n.y + m[2][0] * n.z;
    nr.y = m[0][1] * n.x + m[1][1] * n.y + m[2][1] * n.z;
    nr.z = m[0][2] * n.x + m[1][2] * n.y + m[2][2] * n.z;
    return nr;
}

point_t operator*(mat4 & m, point_t p) {
    point_t pr;
    pr.x = m[0][0] * p.x + m[0][1] * p.y + m[0][2] * p.z + m[0][3];
    pr.y = m[1][0] * p.x + m[1][1] * p.y + m[1][2] * p.z + m[1][3];
    pr.z = m[2][0] * p.x + m[2][1] * p.y + m[2][2] * p.z + m[2][3];
    real w = m[3][0] + m[3][1] + m[3][2] + m[3][3];
    return w == 1 ? pr : (point_t) {pr.x/w, pr.y/w, pr.z/w};
}
