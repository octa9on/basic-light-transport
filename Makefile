CXX = g++
CPPFLAGS = -isystem /mingw64/include/SDL2 -isystem ../Lyra/include -isystem ../pcg-cpp-0.98/include
DEBUGFLAGS = -g -Wall -fdiagnostics-color=always #-Og
ARCHFLAGS = -m64 -march=x86-64 -mavx2 -mtune=native -pthread
OPTFLAGS = -O3 #-fprofile-use
CXXFLAGS = $(DEPFLAGS) $(DEBUGFLAGS) -std=gnu++20 $(ARCHFLAGS) $(OPTFLAGS)
LDFLAGS = -L/mingw64/lib
LDLIBS = -lmingw32 -lSDL2main -lSDL2 -lSDL2_image

DEPDIR = deps
DEPFLAGS = -MMD -MF $(DEPDIR)/$*.d
DEPFILES = $(OBJS:%.o=$(DEPDIR)/%.d)

OBJS = $(patsubst %.cc,%.o,$(wildcard *.cc))

#TODO fix not taking deps/blt.d into account for blt.exe
blt: $(filter-out blt.o,${OBJS})

clean:
	rm -f blt.exe ${OBJS} ${DEPDIR}/*.d

-include $(wildcard $(DEPFILES))
