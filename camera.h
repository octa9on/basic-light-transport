#pragma once

#include "linalg.h"

struct camera_t {
    point_t lens_center;
    point_t proj_center;
    vector_t right;
    vector_t up;

    camera_t() {}
    camera_t(point_t pos, point_t look, real fov_deg=60, vector_t vert={0,1,0});

    point_t window_to_proj(real x, real y);
    virtual ray_t pixel_ray(real x, real y);
};

struct dof_camera_t : camera_t {
    real aperture;
    real focal_length;

    dof_camera_t() {}
    dof_camera_t(point_t pos, point_t look, real ap, real fl=0,
                 real fov_deg=60, vector_t vert={0,1,0});
    dof_camera_t(point_t pos, point_t look, real ap, point_t fp,
                 real fov_deg=60, vector_t vert={0,1,0});
    ray_t pixel_ray(real x, real y);
};
