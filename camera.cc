#include <cmath>

#include "blt.h"
#include "camera.h"

camera_t::camera_t(point_t pos, point_t look, real fov_deg, vector_t vert)
        : lens_center(pos) {
    if (cross(look - pos, vert).length() == 0) {
        die("vertical camera pose requires explicit up-vector");
    }

    real fov = fov_deg * M_PI / 180;
    real aspect_ratio = real(WINDOW_WIDTH) / real(WINDOW_HEIGHT);
    vector_t forward = (look - lens_center).normalize();
    right = cross(vert, forward).normalize() * aspect_ratio;
    up = cross(forward, right).normalize();
    proj_center = lens_center + forward / tan(fov/2);
}

point_t camera_t::window_to_proj(real x, real y) {
    return proj_center
           + right * (x*2 / WINDOW_WIDTH - 1.0)
           + up * (1.0 - y*2 / WINDOW_HEIGHT);
}

ray_t camera_t::pixel_ray(real x, real y) {
    return {lens_center, (window_to_proj(x, y) - lens_center).normalize()};
}

dof_camera_t::dof_camera_t(point_t pos, point_t look, real ap, real fl,
                           real fov_deg, vector_t vert)
        : camera_t(pos, look, fov_deg, vert), aperture(ap), focal_length(fl) {
    if (! focal_length) focal_length = (look - pos).length();
}

dof_camera_t::dof_camera_t(point_t pos, point_t look, real ap, point_t fp,
                           real fov_deg, vector_t vert)
        : camera_t(pos, look, fov_deg, vert), aperture(ap) {
    focal_length = (fp - pos).length();
}

//TODO make the "focal sphere" into a proper focal plane
ray_t dof_camera_t::pixel_ray(real x, real y) {
    point_t focal_point = lens_center + (window_to_proj(x, y) - lens_center).with_length(focal_length);
    vector_t jitter;
    do jitter = vector_t({r11(gen), r11(gen), 0});
    while (jitter.sq_length() > 1);
    vector_t dxy = (right * jitter.x + up * jitter.y) * aperture/2;
    point_t lens_point = lens_center + dxy;
    return {lens_point, (focal_point - lens_point).normalize()};
}
