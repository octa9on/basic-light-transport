#pragma once

#include <cfloat>

using std::clamp;

using real = float;
constexpr real REAL_MIN = FLT_MIN;
constexpr real REAL_MAX = FLT_MAX;
//using real = double;
//constexpr real REAL_MIN = DBL_MIN;
//constexpr real REAL_MAX = DBL_MAX;

inline real clamp01(real n) {return clamp(n, real(0), real(1));}
