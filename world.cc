#include <functional>

extern "C" {
#include <SDL.h>
#include <SDL_image.h>
}

#include "render.h"
#include "world.h"

using std::hypot;
using std::max;

texture_t::texture_t(string filename) {
    SDL_RWops * rwop = SDL_RWFromFile(filename.c_str(), "rb");
    string filetype = filename.substr(filename.size()-3);
    for (auto & c : filetype) c = toupper(c);
    SDL_Surface * image = IMG_LoadTyped_RW(rwop, true, filetype.c_str());
    if (! image) die("can't load texture " + filename);

    SDL_Surface * i = SDL_ConvertSurfaceFormat(image, IMG_FORMAT, 0);
    SDL_FreeSurface(image);

    SDL_LockSurface(i);
    width = i->w;
    height = i->h;
    texels.resize(height);
    uint8_t * pixels = static_cast<uint8_t *>(i->pixels);
    for (int y=0 ; y<i->h ; y+=1) {
        texels[y].resize(width);
        auto & texel_row = texels[y];
        uint8_t * pixel_row = pixels + y * i->pitch;
        for (int x=0 ; x<i->w ; x+=1) {
            auto & texel = texel_row[x];
            uint8_t * pixel = pixel_row + x * 4;
            texel.red = pixel[RED] / 255.;
            texel.green = pixel[GREEN] / 255.;
            texel.blue = pixel[BLUE] / 255.;
        }
    }
    SDL_FreeSurface(i);
}

color_t texture_t::texel(real hit_u, real hit_v) {
    int u = (1 + hit_u)/2 * (width-1) + 0.5; //XXX wrap properly (mod width?)
    int v = (1 - hit_v)/2 * (height-1) + 0.5;
    return texels[v][u];
}

// thought this was not uniform, turns out it is
inline pair<vector_t, real> sphere_sample() {
    real z = r11(gen);
    real r = sqrt(1 - sq(z));
    real phi = r0tau(gen);
    return {{r * cos(phi), r * sin(phi), z}, 1 / (4*M_PI)};
}

inline pair<vector_t, real> hemisphere_sample() {
    real z = r01(gen);
    real r = sqrt(1 - sq(z));
    real phi = r0tau(gen);
    return {{r * cos(phi), r * sin(phi), z}, 1 / (2*M_PI)};
}

inline vector_t cosine_hemisphere_sample(vector_t v) {
    real r = sqrt(r01(gen));
    real phi = r0tau(gen);
    vector_t dv = {r * cos(phi), r * sin(phi), sqrt(1 - sq(r))};

    return dv.rotate(vector_t({0,0,1}), v);
}

pair<color_t, real> lambertian::rfl(shape_hit_t * hit, vector_t from) {
    return {color / M_PI, dot(hit->nml, - from) / M_PI};
}

tuple<vector_t, color_t, real> lambertian::sample_rfl(shape_hit_t * hit) {
    vector_t light_to = cosine_hemisphere_sample(mkvector_t(hit->nml));
    auto [color, pdf] = rfl(hit, - light_to);
    return {- light_to, color, pdf};
}

pair<color_t, real> lambertian_map::rfl(shape_hit_t * hit, vector_t from) {
    color_t color = texture->texel(hit->u, hit->v);
    return {color / M_PI, dot(hit->nml, - from) / M_PI};
}

pair<color_t, real> blend_map::rfl(shape_hit_t * hit, vector_t from) {
    int which = r01(gen) < texture->texel(hit->u, hit->v).green;
    if (which == 0) return shader1->rfl(hit, from);
    else return shader2->rfl(hit, from);
}

tuple<vector_t, color_t, real> blend_map::sample_rfl(shape_hit_t * hit) {
    int which = r01(gen) < texture->texel(hit->u, hit->v).green;
    if (which == 0) return shader1->sample_rfl(hit);
    else return shader2->sample_rfl(hit);
}

vector_t reflect(vector_t incident, normal_t normal) {
    return incident + normal * 2 * dot(- incident, normal);
}

pair<color_t, real> perfect_mirror::rfl(shape_hit_t * hit, vector_t from) {
    return {{0,0,0}, 0}; // infinitessimal specular lobe can't be hit by chance
}

tuple<vector_t, color_t, real> perfect_mirror::sample_rfl(shape_hit_t * hit) {
    return {- reflect(hit->inc, hit->nml.normalize()), {1,1,1}, REAL_MAX};
}

pair<color_t, real> emissive::rfl(shape_hit_t * hit, vector_t from) {
    return {{0,0,0}, 0};
}

tuple<vector_t, color_t, real> emissive::sample_rfl(shape_hit_t * hit) {
    return {{0,0,0}, {0,0,0}, 0};
}

color_t emissive_map::emission(shape_hit_t * hit) {
    color_t raw_color = texture->texel(hit->u, hit->v);
    color_t color = raw_color * scale;
    return color;
}

pair<color_t, real> emissive_map::rfl(shape_hit_t * hit, vector_t from) {
    return shader->rfl(hit, from);
}

tuple<vector_t, color_t, real> emissive_map::sample_rfl(shape_hit_t * hit) {
    return shader->sample_rfl(hit);
}

void scene_t::add(object_t * o) {
    if (o->emits()) lights.push_back(o);
    else items.push_back(o);
}

void scene_t::add(vector<object_t *> os) {
    for (auto o : os) add(o);
}

optional<hit_t> scene_t::trace_ray(ray_t ray) {
    optional<hit_t> best_hit = nullopt;
    for (auto * item : items) {
        optional<hit_t> hit = item->trace_ray(ray);
        if (hit && (! best_hit || hit->dist < best_hit->dist)) {
            best_hit = hit;
        }
    }
    for (auto * item : lights) {
        optional<hit_t> hit = item->trace_ray(ray);
        if (hit && (! best_hit || hit->dist < best_hit->dist)) {
            best_hit = hit;
        }
    }

    return best_hit;
}

tuple<object_t *, color_t, vector_t, real> scene_t::sample_light_at(hit_t * hit) {
    if (lights.empty()) return {nullptr, {0,0,0}, {0,0,0}, 0};

    std::uniform_int_distribution<int> ri(0, lights.size()-1);
    int ix = ri(gen);
    hit_t l_hit = lights[ix]->sample_object(hit->pos.skootch(hit->nml));
    vector_t from = hit->pos - l_hit.pos;
    color_t radiance = {0,0,0};
    real geom = 0;
    if (path_clear(hit, & l_hit)) {
        radiance = l_hit.shader->emission(& l_hit);
        real sq_len = from.sq_length();
        vector_t from_normalized = from / sqrt(sq_len);
        geom = dot(hit->nml, - from_normalized)
               * dot(l_hit.nml, from_normalized)
               / sq_len; //XXX wrong?
    }
    return {l_hit.object, radiance * geom, - l_hit.inc, l_hit.pdf / lights.size()};
}

bool scene_t::path_clear(hit_t * from_hit, hit_t * to_hit) {
    point_t from = from_hit->pos.skootch(from_hit->nml);
    if (to_hit->exit) {
        optional<hit_t> hit = trace_ray(ray_t({from, to_hit->inc}));
        //TODO find out why path_clear called when no skysphere
        if (! hit) warn("path_clear didn't hit");
        return hit->exit;
    }

    point_t to = to_hit->pos.skootch(to_hit->nml);
    vector_t path_v = to - from;
    real dist = path_v.length();
    optional<hit_t> hit = trace_ray(ray_t({from, path_v}));
    if (! hit) warn("path_clear didn't hit");
    return hit->dist >= dist;
}

void transform(transform_t & t, vector<object_t *> & ros, object_t * o) {
    o->update_pose(t);
    ros.push_back(o);
}

void transform(transform_t & t, vector<object_t *> & ros, vector<object_t *> os) {
    for (auto o : os) {
        o->update_pose(t);
        ros.push_back(o);
    }
}

void object_t::update_pose(transform_t t) {
    //if (! pose) die("null pose");
    //if (! t) die("null transform");
    pose = t * pose;
}

optional<hit_t> object_t::trace_ray(ray_t world_ray) {
    ray_t ray = world_ray.from_world(pose);
    optional<shape_hit_t> shape_hit = shape->trace_ray(ray);
    if (! shape_hit) return nullopt;
    shape_hit->update_to_world(pose); //TODO also transform shader coordinates
    shape_hit->dist = (shape_hit->pos - world_ray.src).length(); //XXX yuck
    return hit_t(* shape_hit, this, shader);
}

hit_t object_t::sample_object(point_t world_viewpoint) {
    point_t viewpoint = world_viewpoint.from_world(pose);
    shape_hit_t shape_hit = shape->sample_shape(viewpoint);
    shape_hit.update_to_world(pose); //TODO also transform shader coordinates
    shape_hit.dist = (shape_hit.pos - world_viewpoint).length(); //XXX yuck
    return hit_t(shape_hit, this, shader);
}

void shape_hit_t::update_to_world(transform_t & t) {
    pos = pos.to_world(t);
    inc = inc.to_world(t);
    nml = nml.to_world(t);
}

// u=+1 -> +z ; u=+1/2 -> +x ; u=0 -> -z
// v=-1 -> -y pole ; v=+1 -> +y pole
template<typename T>
pair<real, real> uv(T p) {
    real u = atan2(p.x, -p.z) / M_PI;
    real v = asin(p.y) * 2/M_PI;
    return {u, v};
}

optional<shape_hit_t> skysphere_s::trace_ray(ray_t ray) {
    auto [u, v] = uv(ray.dir);
    real pdf = 1/(4*M_PI);
    point_t p = (point_t) {0,0,0} + ray.dir.with_length(REAL_MAX);
    shape_hit_t hit(REAL_MAX, p, ray.dir, mknormal_t(- ray.dir), u,v, pdf);
    hit.exit = true;
    return hit;
}

shape_hit_t skysphere_s::sample_shape(point_t viewpoint) {
    auto [dir, pdf] = sphere_sample();
    auto [u, v] = uv(dir);
    point_t p = (point_t) {0,0,0} + dir.with_length(REAL_MAX);
    shape_hit_t hit(REAL_MAX, p, dir, mknormal_t(- dir), u,v, pdf);
    hit.exit = true;
    return hit;
}

real skysphere_s::areal_pdf(shape_hit_t * hit) {
    return 0;
}

optional<shape_hit_t> triangle_s::trace_ray(ray_t ray) {
    normal_t normal = cross_n(p2 - p1, p3 - p1);
    vector_t pvec = ray.src - p1;
    vector_t dir = ray.dir.normalize();
    real t = - dot(normal, pvec) / dot(normal, dir);
    if (t < 0) return nullopt;
    point_t contact = ray.src + dir * t;

    if (dot(normal, cross(p2 - p1, contact - p1)) < 0
        || dot(normal, cross(p3 - p2, contact - p2)) < 0
        || dot(normal, cross(p1 - p3, contact - p3)) < 0) return nullopt;
    else {
        normal_t eff_normal = dot(ray.dir, normal) < 0 ? normal : -normal;
        real pdf = -1; //XXX TODO
        real u=0, v=0; //TODO
        return shape_hit_t(t, contact, ray.dir, eff_normal, u,v, pdf);
    }
}

shape_hit_t triangle_s::sample_shape(point_t viewpoint) {
    return {0, p1, {0,0,0}, cross_n(p2-p1, p3-p1), 0,0, 0}; // XXX TODO
}

real triangle_s::areal_pdf(shape_hit_t * hit) {
    return 1; //XXX TODO
}

inline real uniform_cone_pdf(real cos_theta_max) {
    return 1 / (2*M_PI * (1 - cos_theta_max));
}

real sphere_pdf(point_t ctr, real r, point_t viewpoint) {
    real sin_theta_max2 = sq(r) / (viewpoint - ctr).sq_length();
    real cos_theta_max = sqrt(max(real(0), 1 - sin_theta_max2));
    real pdf = uniform_cone_pdf(cos_theta_max);
    return pdf;
}

optional<shape_hit_t> sphere_s::trace_ray(ray_t ray) {
    point_t src = ray.src;
    vector_t dir = ray.dir.normalize();
    ray = {src, dir}; // normalize ray because hours of frustration

    real sq_r = sq(r);
    vector_t stc = ctr - src;
    real stc_sq_len = stc.sq_length();

    shape_hit_t hit;
    if (dot(stc, dir) < 0) {
        // sphere center behind ray source
        if (stc_sq_len > sq_r) return nullopt; // outside sphere
        else if (stc_sq_len == sq_r) { // on sphere
            auto normal = mknormal_t(- stc.normalize());
            auto [u, v] = uv(normal);
            hit = shape_hit_t(0, src, ray.dir, normal, u,v, -1);
        }
        else { // inside sphere
            point_t proj = project_onto(ctr, ray);
            real d1 = sqrt(sq_r - (proj - ctr).sq_length());
            real d2 = d1 - (proj - src).length();
            point_t tgt = src + dir * d2;
            auto normal = mknormal_t((tgt - ctr).normalize());
            auto [u, v] = uv(normal);
            hit = shape_hit_t(d2, tgt, ray.dir, normal, u,v, -1);
        }
    }
    else {
        // ray source behind sphere center
        point_t proj = project_onto(ctr, ray);
        if ((ctr - proj).sq_length() > sq_r) return nullopt; //misses sphere
        else {
            real d1 = sqrt(sq_r - (proj - ctr).sq_length());
            real d2 = (proj - src).length();
            if (stc_sq_len > sq_r) d2 -= d1; // outside sphere
            else d2 += d1; // inside sphere
            point_t tgt = src + dir * d2;
            auto normal = mknormal_t((tgt - ctr).normalize());
            auto [u, v] = uv(normal);
            hit = shape_hit_t(d2, tgt, ray.dir, normal, u,v, -1);
        }
    }
    hit.pdf = sphere_pdf(ctr, r, src);
    return hit;
}

/*
//XXX this is badly mathed
// you aren't kidding... :-/ oy
shape_hit_t sphere_s::sample_shape(point_t viewpoint) {
    vector_t temp = viewpoint - ctr;
    real len = temp.length();
    vector_t dir = temp / len;
    auto [sphs, _] = sphere_sample();
    point_t pt = ctr + (dir + sphs).with_length(r + SKOOTCH_FACTOR);

    real pdf = sphere_pdf(ctr, r, viewpoint);

    auto [u, v] = uv(sphs); //XXX generate u,v first then map onto sphere
    return {len - r, pt, - dir, mknormal_t(dir), u,v, pdf};
}
*/

shape_hit_t sphere_s::sample_shape(point_t viewpoint) {
    auto [dir, pdf] = sphere_sample();
    point_t point = ctr + dir*r;
    vector_t incoming = point - viewpoint;
    auto [u,v] = uv(dir);
    return {incoming.length(), point, incoming, mknormal_t(dir), u,v, pdf};
}

real sphere_s::areal_pdf(shape_hit_t * hit) {
    real sin_theta_max2 = sq(r) / (hit->pos - ctr).sq_length();
    real cos_theta_max = sqrt(max(real(0), 1 - sin_theta_max2));
    real pdf = uniform_cone_pdf(cos_theta_max);
    return pdf;
}

constexpr real tiny = 0.0001;

template<typename T>
normal_t est_nml(T fn, point_t p) {
    array<vector_t, 4> ds = {(vector_t) {1,-1,-1}, {-1,-1,1}, {-1,1,-1}, {1,1,1}};
    vector_t sum = {0,0,0};
    for (auto d : ds) {
        sum += d * fn(p + d * tiny);
    }
    return mknormal_t(sum);
}

template<>
normal_t est_nml(function<real(real, real)> fn, point_t p) {
    array<vector_t, 4> ds = {(vector_t) {1,-1,-1}, {-1,-1,1}, {-1,1,-1}, {1,1,1}};
    vector_t sum = {0,0,0};
    for (auto d : ds) {
        point_t pp = p + d * tiny;
        sum += d * fn(pp.y, hypot(pp.x, pp.z));
    }
    return mknormal_t(sum);
}

constexpr point_t eggthing_bound_ctr = {0,sqrt(M_E),0};
constexpr real eggthing_bound_r = sqrt(M_E) - 1 / (2 * M_E);

eggthing_s::eggthing_s() : bound(eggthing_bound_ctr, eggthing_bound_r) {
}

auto eggthing_fn = [](point_t p) -> real {
    return sqrt(sq(p.x) + sq(log(p.y)) + sq(p.z)) - 1;
};

optional<shape_hit_t> eggthing_s::trace_ray(ray_t ray) {
    if (eggthing_fn(ray.src) <= 0) return nullopt; //XXX viewpoint inside

    vector_t unit = ray.dir.normalize();

    auto in_bound = [&](point_t p) -> bool {
        return (bound.ctr - p).sq_length() <= sq(bound.r);
    };

    real est;
    if (! in_bound(ray.src)) {
        auto bound_hit = bound.trace_ray(ray);
        if (! bound_hit) return nullopt;
        est = bound_hit->dist;
    }
    else est = eggthing_fn(ray.src);

    real val, steps=100;
    do {
        est += val = eggthing_fn(ray.src + unit * est);
        if (steps-- < 0 || ! in_bound(ray.src + unit * est)) return nullopt;
    } while (val > tiny);

    point_t tgt = ray.src + unit * est;
    normal_t nml = est_nml(eggthing_fn, tgt);
    auto [u, v] = uv((tgt - (point_t) {0,(exp(1)-exp(-1))/2,0}).normalize());
    real pdf = 1 / (4 * M_PI); //XXX WRONG
    return shape_hit_t(est, tgt, unit, nml, u,v, pdf);
}

shape_hit_t eggthing_s::sample_shape(point_t viewpoint) {
    auto [dir, pdf] = sphere_sample();
    auto [u,v] = uv(dir);
    auto nml = mknormal_t(dir); //XXX fix this
    point_t point = (point_t) {0,0,0} + dir;
    point.y = exp(point.y);
    vector_t incoming = point - viewpoint;
    return {incoming.length(), point, incoming, nml, u,v, pdf};
}

real eggthing_s::areal_pdf(shape_hit_t * hit) {
    return -1; //XXX
}

constexpr point_t thing_bound_ctr = {0,-1,0};
constexpr real thing_bound_r = 2;

thing_s::thing_s() : bound(thing_bound_ctr, thing_bound_r) {
}

auto thing_fn = [](point_t p) -> real {
    p.y += 1;
    real k0=1, k1=-1, k2=1, k3=-1;
    real r = 1;
    real sumsq = sq(p.x) + sq(p.y) + sq(p.z);
    real dodeca = six(p.z) - 5*(sq(p.x) + sq(p.y)) * four(p.z)
                  + 5 * sq(sq(p.x) + sq(p.y)) * sq(p.z)
                  - 2 * (four(p.x) - 10 * sq(p.x) * sq(p.y) + 5 * four(p.y))
                    * p.x * p.z
                  + k0 * cb(sumsq) + k1 * sq(r) * sq(sumsq)
                  + k2 * four(r) * sumsq + k3 * six(r);
    return min(dodeca, hypot(p.x,p.y,p.z)-real(sq(2/3.)));
};

optional<shape_hit_t> thing_s::trace_ray(ray_t ray) {
    if (thing_fn(ray.src) <= 0) return nullopt; //XXX viewpoint inside

    vector_t unit = ray.dir.normalize();

    auto in_bound = [&](point_t p) -> bool {
        return (bound.ctr - p).sq_length() <= sq(bound.r);
    };

    real est;
    if (! in_bound(ray.src)) {
        auto bound_hit = bound.trace_ray(ray);
        if (! bound_hit) return nullopt;
        est = bound_hit->dist;
    }
    else est = thing_fn(ray.src);
    real p_est = est;

    real val, steps=100;
    do {
        p_est = est;
        val = thing_fn(ray.src + unit * est);
        est += val;
        if (steps-- < 0 || ! in_bound(ray.src + unit * est)) return nullopt;
    } while (val > tiny);

    real far = est;
    real near = p_est;
    while (far > near+tiny) {
        est = near + (far - near)/2;
        val = thing_fn(ray.src + unit * est);
        if (val > 0) near = est;
        else far = est;
    }
    est = far;

    point_t tgt = ray.src + unit * est;
    normal_t nml = est_nml(thing_fn, tgt);
    auto [u, v] = uv((tgt - bound.ctr).normalize());
    real pdf = 1 / (4 * M_PI); //XXX WRONG
    return shape_hit_t(est, tgt, unit, nml, u,v, pdf);
}

shape_hit_t thing_s::sample_shape(point_t viewpoint) {
    auto [dir, pdf] = sphere_sample();
    auto [u,v] = uv(dir);
    auto nml = mknormal_t(dir); //XXX fix this
    point_t point = (point_t) {0,0,0} + dir;
    point.y = exp(point.y);
    vector_t incoming = point - viewpoint;
    return {incoming.length(), point, incoming, nml, u,v, pdf};
}

real thing_s::areal_pdf(shape_hit_t * hit) {
    return -1; //XXX
}

optional<shape_hit_t> revsolid_s::trace_ray(ray_t ray) {
    if (fn(ray.src.y, hypot(ray.src.x, ray.src.z)) <= 0) return nullopt; //XXX viewpoint inside

    vector_t unit = ray.dir.normalize();

    auto in_bound = [&](point_t p) -> bool {
        return (bound.ctr - p).sq_length() <= sq(bound.r);
    };

    real est;
    if (! in_bound(ray.src)) {
        auto bound_hit = bound.trace_ray(ray);
        if (! bound_hit) return nullopt;
        est = bound_hit->dist;
    }
    else est = fn(ray.src.y, hypot(ray.src.x, ray.src.z));
    real p_est = est;

    real val, steps=100;
    do {
        p_est = est;
        point_t p = ray.src + unit * est;
        val = fn(p.y, hypot(p.x, p.z));
        est += val;
        if (steps-- < 0 || ! in_bound(ray.src + unit * est)) return nullopt;
    } while (val > tiny/10);

    real far = est;
    real near = p_est;
    while (far > near+tiny/10) {
        est = near + (far - near)/2;
        point_t p = ray.src + unit * est;
        val = fn(p.y, hypot(p.x, p.z));
        if (val > 0) near = est;
        else far = est;
    }
    est = far;

    point_t tgt = ray.src + unit * est;
    normal_t nml = est_nml(fn, tgt).normalize();
    auto [u, v] = uv((tgt - bound.ctr).normalize());
    real pdf = 1 / (4 * M_PI); //XXX WRONG
    return shape_hit_t(est, tgt, unit, nml, u,v, pdf);
}

shape_hit_t revsolid_s::sample_shape(point_t viewpoint) {
    return {}; //XXX
}

real revsolid_s::areal_pdf(shape_hit_t * hit) {
    return -1; //XXX
}

optional<shape_hit_t> plane_s::trace_ray(ray_t ray) {
    vector_t dir = ray.dir.normalize();
    real temp = dot(nml, dir);
    if (temp == 0) return nullopt; // ray parallel with plane
    real t = dot(nml, pt - ray.src) / temp;
    if (t < 0) return nullopt; // ray away from plane
    point_t contact = ray.src + dir * t;
    real pdf = -1; //XXX TODO
    real u=0, v=0; //XXX TODO
    return shape_hit_t(t, contact, ray.dir, nml, u,v, pdf);
}

shape_hit_t plane_s::sample_shape(point_t viewpoint) {
    vector_t from = pt - viewpoint;
    return {from.length(), pt, from, nml, 0,0, 1}; // XXX TODO
}

real plane_s::areal_pdf(shape_hit_t * hit) {
    return 1; //XXX TODO
}

optional<shape_hit_t> one_sided_plane_s::trace_ray(ray_t ray) {
    vector_t dir = ray.dir.normalize();
    real temp = dot(nml, dir);
    if (temp >= 0) return nullopt; // ray parallel with or behind plane
    real t = dot(nml, pt - ray.src) / temp;
    if (t < 0) return nullopt; // ray away from plane
    point_t contact = ray.src + dir * t;
    real pdf = -1; //XXX TODO
    real u=0, v=0; //XXX TODO
    return shape_hit_t(t, contact, ray.dir, nml, u,v, pdf);
}

shape_hit_t one_sided_plane_s::sample_shape(point_t viewpoint) {
    return {-1, pt, {0,0,0}, nml, 0,0, 0}; // XXX TODO
}

real one_sided_plane_s::areal_pdf(shape_hit_t * hit) {
    return 1; //XXX TODO
}

box_s::box_s(point_t c, real x, real y, real z)
        : ctr(c), xr(x), yr(y), zr(z) {
    face_nml = {
        (normal_t){xr,0,0}, (normal_t){-xr,0,0},
        (normal_t){0,yr,0}, (normal_t){0,-yr,0},
        (normal_t){0,0,zr}, (normal_t){0,0,-zr},
    };

    for (int ix=0 ; ix<6 ; ix+=1) {
        face_ctr[ix] = ctr + face_nml[ix];
        face_nml[ix] = face_nml[ix].normalize();
    }

    face_u = {
        mkvector_t(face_nml[4]), mkvector_t(face_nml[5]), // +z, -z
        mkvector_t(face_nml[0]), mkvector_t(face_nml[1]), // +x, -x
        mkvector_t(face_nml[2]), mkvector_t(face_nml[3]), // +y, -y
    };
    face_ur = {zr, zr, xr, xr, yr, yr};
    face_v = {
        mkvector_t(face_nml[2]), mkvector_t(face_nml[3]), // +y, -y
        mkvector_t(face_nml[4]), mkvector_t(face_nml[5]), // +z, -z
        mkvector_t(face_nml[0]), mkvector_t(face_nml[1]), // +x, -x
    };
    face_vr = {yr, yr, zr, zr, xr, xr};
}

inline real tri_angular_size(array<vector_t, 3> vertex) {
    vector_t v01 = cross(vertex[0], vertex[1]);
    vector_t v12 = cross(vertex[1], vertex[2]);
    vector_t v20 = cross(vertex[2], vertex[0]);

    if (v01.sq_length() > 0) v01 = v01.normalize();
    if (v12.sq_length() > 0) v12 = v12.normalize();
    if (v20.sq_length() > 0) v20 = v20.normalize();

    return abs(acos(dot(v01, -v12))
               + acos(dot(v12, -v20))
               + acos(dot(v20, -v01))
               - M_PI);
}

inline real tri_angular_size(point_t viewpoint, array<point_t, 3> corner) {
    // unit vector to each corner
    array<vector_t, 3> uvec;
    for (int ix=0 ; ix<3 ; ix+=1) {
        uvec[ix] = (corner[ix] - viewpoint).normalize();
    }

    return tri_angular_size({uvec[0], uvec[1], uvec[2]});
}

inline real quad_angular_size(point_t viewpoint, array<point_t, 4> corner) {
    // unit vector to each corner
    array<vector_t, 4> uvec;
    for (int ix=0 ; ix<4 ; ix+=1) {
        uvec[ix] = (corner[ix] - viewpoint).normalize();
    }

    return tri_angular_size({uvec[0], uvec[1], uvec[2]})
           + tri_angular_size({uvec[2], uvec[3], uvec[0]});
}

optional<shape_hit_t> box_s::trace_ray(ray_t ray) {
    int best_ix = -1;
    real best_k = REAL_MAX;
    point_t best_hit;
    real best_u=NAN, best_v=NAN;
    int seen = 0;
    array<real, 6> area;
    for (int ix=0 ; ix<6 ; ix+=1) {
        real temp1 = dot(face_nml[ix], ray.src - face_ctr[ix]);
        if (temp1 < 0) continue;
        seen += 1;
        area[ix] = 4 * face_ur[ix] * face_vr[ix];
        real temp2 = dot(face_nml[ix], ray.dir);
        real k = - temp1 / temp2;
        if (k > 0 && k < best_k) {
            point_t hit_point = ray.src + ray.dir * k;
            vector_t hit_vec = hit_point - face_ctr[ix];
            real hit_u = dot(face_u[ix], hit_vec);
            real hit_v = dot(face_v[ix], hit_vec);
            if (abs(hit_u) <= face_ur[ix]
                && abs(hit_v) <= face_vr[ix]) {
                best_ix = ix;
                best_k = k;
                best_hit = hit_point;
                best_u = hit_u;
                best_v = hit_v;
            }
        }
    }

    if (best_ix == -1) return nullopt; //XXX TODO viewpoint inside box
    else {
        real pdf = 1 / (area[best_ix] * seen);
        real u = best_u / face_ur[best_ix]; //XXX TODO properly unwrap box
        real v = best_v / face_vr[best_ix]; //XXX TODO
        return (shape_hit_t){ray.dir.length() * best_k, best_hit, ray.dir,
                             face_nml[best_ix], u,v, pdf};
    }
}

shape_hit_t box_s::sample_shape(point_t viewpoint) {
    // find visible faces
    int cix = -1;
    int seen = 0;
    array<bool, 6> visible;
    array<real, 6> area;
    for (int ix=0 ; ix<6 ; ix+=1) {
        visible[ix] = dot(face_nml[ix], viewpoint - face_ctr[ix]) > 0;
        if (! visible[ix]) continue;
        std::uniform_int_distribution<int> ri(0, seen);
        if (ri(gen) == 0) cix = ix;
        seen += 1;
        area[ix] = 4 * face_ur[ix] * face_vr[ix];
    }
    if (seen == 0) {
        return (shape_hit_t) {0, {0,0,0}, {0,0,0}, {0,0,0}, 0,0, 0};
        die("viewpoint inside box"); //XXX
        //TODO viewpoint inside box
        cix = std::uniform_int_distribution<int>(0, 5)(gen);
        seen = 6;
    }

    // select point on face
    real u = r11(gen);
    real v = r11(gen);
    point_t pt = face_ctr[cix] + face_u[cix] * u * face_ur[cix]
                               + face_v[cix] * v * face_vr[cix];

    // pdf with respect to face area
    real pdf = 1 / (area[cix] * seen);
    vector_t inc = pt - viewpoint;
    real dist = inc.length();

    return (shape_hit_t){dist, pt, inc, face_nml[cix], u,v, pdf};
}

real box_s::areal_pdf(shape_hit_t * hit) {
    // find visible faces
    int cix = -1;
    int seen = 0;
    array<bool, 6> visible;
    array<real, 6> area;
    for (int ix=0 ; ix<6 ; ix+=1) {
        visible[ix] = dot(face_nml[ix], hit->pos - face_ctr[ix]) > 0;
        if (! visible[ix]) continue;
        std::uniform_int_distribution<int> ri(0, seen);
        if (ri(gen) == 0) cix = ix;
        seen += 1;
        area[ix] = 4 * face_ur[ix] * face_vr[ix];
    }
    if (seen == 0) {
        return 0; //XXX
        // TODO viewpoint inside box
        cix = std::uniform_int_distribution<int>(0, 5)(gen);
        seen = 6;
    }

    // pdf with respect to face area
    return 1 / (area[cix] * seen);
}

/*
optional<shape_hit_t> csg_diff::trace_ray(ray_t ray) {
}
*/

shape_hit_t csg_diff::sample_shape(point_t viewpoint) {
    return {-1, {0,0,0}, {0,0,0}, {0,0,0}, 0,0, 0}; // XXX TODO
}

real csg_diff::areal_pdf(shape_hit_t * hit) {
    return 0; //XXX abandon hope
}
