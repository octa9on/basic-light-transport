#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <thread>

extern "C" {
#include <x86intrin.h>

#include <SDL.h>
}

#include "camera.h"
#include "scene.h"
#include "render.h"

using std::cout, std::endl;
using namespace std::chrono_literals;
using std::ostringstream;
using std::pair;
using std::thread;

using hrclock = std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using rsec = std::chrono::duration<real>;

int MIN_BOUNCES = 3;
int MAX_BOUNCES = 550;

int PIXEL_COUNT = WINDOW_WIDTH * WINDOW_HEIGHT;

vector<pixel_t> pixel_samples(PIXEL_COUNT);

string checkpoint_filename = "render_checkpoint";

void setup_renderdata(bool resume) {
    //render_state
}

//TODO make this atomic
void save_renderdata() {
    //TODO
    //info("checkpoint saved");
}

color_t naive_camera_path_tracing_sample_pixel(int x, int y) {
    color_t lightsum = {0,0,0};
    color_t throughput = {1,1,1};

    real dx = r55(gen);
    real dy = r55(gen);
    ray_t ray = camera->pixel_ray(x+dx, y+dy);

    for (int bounces=0 ; ; bounces+=1) {
        if (bounces >= MAX_BOUNCES) break;

        optional<hit_t> hit = scene.trace_ray(ray);
        if (! hit) break;

        lightsum += throughput * hit->shader->emission(& * hit);
        if (hit->dist == REAL_MAX) break;

        auto [from, rfl, probd] = hit->shader->sample_rfl(& * hit);
        if (probd == 0) break; //TODO check for rfl={0,0,0}

        if (probd == REAL_MAX) throughput *= rfl;
        else throughput *= rfl * dot(- from, hit->nml) / probd;

        ray = {hit->pos.skootch(hit->nml), - from};

        if (bounces > MIN_BOUNCES) {
            real prob = max(real(0.05), 1 - max_of(throughput));
            if (r01(gen) < prob) break;
            throughput /= 1 - prob;
        }
    }

    return lightsum;
}

real power_heuristic(int n_a, real pdf_a, int n_b, real pdf_b) {
    real a = n_a * pdf_a;
    real b = n_b * pdf_b;
    return sq(a) / (sq(a) + sq(b));
}

color_t camera_path_tracing_sample_pixel(int x, int y) {
    again:
    color_t lightsum = {0,0,0};
    color_t throughput = {1,1,1};

    real dx = r55(gen);
    real dy = r55(gen);
    ray_t ray = camera->pixel_ray(x+dx, y+dy);

    bool specular = false;
    for (int bounces=0 ; ; bounces+=1) {
        optional<hit_t> hit = scene.trace_ray(ray);
        if (! hit) break;

        if (bounces == 0 || specular || hit->exit) {
            lightsum += throughput * hit->shader->emission(& * hit);
        }

        if (bounces >= MAX_BOUNCES || hit->exit) break;

        color_t l_direct_light = {0,0,0};
        real l_weight = 0;
        auto [l_obj, l_light, l_from, l_probd] = scene.sample_light_at(& * hit);
        if (l_probd > 0 && ! l_light.no_light()) {
            auto [d_rfl, d_probd] = hit->shader->rfl(& * hit, l_from);
            l_weight = 1; //TODO multiple importance sampling;
            // geometry term is folded into l_light
            l_direct_light += (l_light / l_probd) * d_rfl;
        }

        color_t s_direct_light = {0,0,0};
        real s_weight = 0;
        /*
        {
            auto [s_from, s_rfl, s_probd] = hit->shader->sample_rfl(& * hit);
            s_rfl *= dot(- s_from, hit->nml);
            bool sampled_specular = s_probd == REAL_MAX; //XXX fix this
            if (s_probd > 0) { //TODO check for s_rfl={0,0,0}
                ray_t s_ray = {hit->pos.skootch(hit->nml), - s_from};
                if (sampled_specular) s_weight = 1;
                else {
                    optional<hit_t> l_hit = l_obj->trace_ray(s_ray);
                    real sl_probd = 0;
                    if (l_hit) sl_probd = l_obj->shape->areal_pdf(& * l_hit);
                    if (sl_probd > 0) {
                        s_weight = 1; //TODO multiple importance sampling
                    }
                    else goto nope;
                }

                optional<hit_t> sl_hit = scene.trace_ray(s_ray);
                color_t s_light = {0,0,0};
                if (sl_hit) {
                    if (sl_hit->object == l_obj) {
                        s_light = l_obj->shader->emission(& * sl_hit);
                    }
                }
                else {} //XXX add skysphere light if it's the sampled light?

                s_direct_light += s_rfl * s_light / s_probd;
            }
        }
        nope:
        */

        real weight = l_weight + s_weight;
        if (weight > 0) {
            color_t direct_light = (l_direct_light * l_weight 
                                    + s_direct_light * s_weight)
                                   / weight;
            lightsum += throughput * direct_light;
        }

        auto [from, rfl, probd] = hit->shader->sample_rfl(& * hit);
        if (probd == 0) break; //TODO check for rfl={0,0,0}

        specular = probd == REAL_MAX; //XXX fix this
        if (specular) throughput *= rfl; //XXX fix this
        else throughput *= rfl * dot(- from, hit->nml) / probd;

        ray = {hit->pos.skootch(hit->nml), - from};

        if (bounces > MIN_BOUNCES) {
            real prob = max(real(0.05), 1 - max_of(throughput));
            if (r01(gen) < prob) break;
            throughput /= 1 - prob;
        }
    }

    if (max_of(lightsum) > white_pt*5) goto again;

    return lightsum;
}

/*
color_t PREV_camera_path_tracing_sample_pixel(int x, int y) {
    color_t colorsum = {0,0,0};
    int good_samples = 0;
    color_t direct_emission = {0,0,0};

    real dx = r55(gen);
    real dy = r55(gen);
    ray_t p_ray = camera->pixel_ray(x+dx, y+dy);
    hit_t hit = scene.trace_ray(p_ray);

    if (hit.shader->emits()) {
        direct_emission += hit.shader->emission(& hit);
    }

    if (hit.shader->reflects()) for (int nb=0 ; nb<MAX_BOUNCES ; nb+=1) {
        color_t beta = {1,1,1}; //XXX terrible name TODO name it rightly

        int bounces = 0;
        while (hit.object) {
            if (++ bounces > nb) break;

            if (hit.shader->emits()) {
                //colorsum += beta * hit.shader->emission(& hit);
            }

            if (! hit.shader->reflects()) break;

            auto [rfl_n, light_from_n, _] = hit.shader->sample_rfl(& hit);
            beta *= rfl_n;
            if (beta.small()) break;

            hit = scene.trace_ray({hit.pos.skootch(hit.nml), - light_from_n});
        }

        // direct lighting for last leg
        if (! hit.object) continue;
        auto [light, radiance, light_from, l_pdf] = scene.sample_light_at(& hit);
        if (l_pdf > 0) {
            color_t rfl = hit.shader->rfl(& hit, light_from);
            rfl *= dot(- light_from, hit.nml);
            colorsum += beta * (radiance * rfl / l_pdf);
            good_samples += 1;
        }
    }

    return direct_emission + (good_samples ? colorsum / good_samples : (color_t) {0,0,0});
}
*/

int SAMPLES = -1;

bool hasnan(color_t c) {
    return isnan(c.red) || isnan(c.green) || isnan(c.blue);
}

bool naive = false;

void progressive_render_pixel(int pix, int x, int y, uint8_t * pixel) {
    color_t colorsamp;
    int tries=0;
    do {
        if (++ tries > 5) die("persistant NaN");
        if (naive) colorsamp = naive_camera_path_tracing_sample_pixel(x, y);
        else colorsamp = camera_path_tracing_sample_pixel(x, y);
    } while (hasnan(colorsamp)); // re-try if NaN encountered

    auto & s_pix = pixel_samples[pix];
    color_t prev_mean = s_pix.samples ? s_pix.sum / s_pix.samples : colorsamp;
    s_pix.sum += colorsamp;
    s_pix.samples += 1;
    color_t mean = s_pix.sum / s_pix.samples;
    s_pix.ssqdiff += (colorsamp - prev_mean) * (colorsamp - mean);
    color_t var = s_pix.ssqdiff / s_pix.samples;
    /*real E =*/ s_pix.errest = 2 * sqrt(max_of(var) / s_pix.samples);

    //color_t es = {E,E,E};

    pixel[BLUE] = mean.blue8();
    pixel[GREEN] = mean.green8();
    pixel[RED] = mean.red8();
    pixel[ALPHA] = 255;
}

vector<int> scix(PIXEL_COUNT);
void shuffle_pixels() {
    for (int ix=0 ; ix<PIXEL_COUNT ; ix+=1) scix[ix] = ix;
    for (int ix=0 ; ix<PIXEL_COUNT-1 ; ix+=1) {
        std::uniform_int_distribution<int> ri(ix, PIXEL_COUNT-1);
        int rix = ri(gen);
        std::swap(scix[ix], scix[rix]);
    }
}

vector<int> pix, pixb, pixe;

vector<int> samp;

void progressive_render_pixels(int * pix, int pixb, int pixe, uint64_t when,
                               int * samp, uint8_t * pixels, int pitch) {
    again: //XXX fix this ugly control flow
    while (__rdtsc() < when && * pix < pixe && * samp < SAMPLES) {
        int scatter = scix[(* pix)++];
        int x = scatter % WINDOW_WIDTH;
        int y = scatter / WINDOW_WIDTH;
        uint8_t * pixel = pixels + y*pitch + x*4;

        progressive_render_pixel(* pix, x, y, pixel);
    }
    if (__rdtsc() < when && * pix >= pixe && * samp < SAMPLES) {
        * samp += 1;
        * pix = pixb;
        goto again;
    }
}

void precolor() {
    /*
    uint8_t * pixels; int pitch;
    void * temp;
    SDL_LockTexture(texture, nullptr, & temp, & pitch);
    pixels = static_cast<uint8_t *>(temp);
    for (int ix=0 ; ix<PIXEL_COUNT ; ix+=1) {
        int x = ix % WINDOW_WIDTH;
        int y = ix / WINDOW_WIDTH;
        uint8_t * pixel = pixels + y*pitch + x*4;
        pixel[BLUE] = 63;
        pixel[GREEN] = 0;
        pixel[RED] = 63;
        pixel[ALPHA] = 255;
    }
    SDL_UnlockTexture(texture);
    SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);
    SDL_RenderFillRect(renderer, nullptr);
    SDL_RenderCopy(renderer, texture, nullptr, nullptr);
    SDL_RenderPresent(renderer);
    */
    SDL_FillRect(surface, nullptr, SDL_MapRGB(surface->format, 255,0,255));
    SDL_UpdateWindowSurface(window);
}

constexpr real minute = 60;
constexpr real hour = 60 * minute;
constexpr real day = 24 * hour;

string fmt_time(real raw_sec) {
    ostringstream time;
    real sec = raw_sec;
    if (raw_sec > day) {
        int days = sec / day;
        time << days << " d ";
        sec -= days * day;
    }

    if (raw_sec > hour) {
        int hours = sec / hour;
        time << hours << " h ";
        sec -= hours * hour;
    }

    if (raw_sec > minute) {
        int minutes = sec / minute;
        time << minutes << " m ";
        sec -= minutes * minute;
    }

    time << std::setprecision(3) << std::fixed;
    time << sec << " s";

    return time.str();
}

constexpr uint64_t S_TICKS = 400'000'000; // cycles per paint: ~100ms @ ~4GHz
constexpr uint64_t TICKS = 4'000'000'000; // cycles per paint: ~1000ms @ ~4GHz
void render_scene(int threads, int samples) {
    SAMPLES = samples;

    shuffle_pixels();

    precolor();

    pix.resize(threads);
    pixb.resize(threads);
    pixe.resize(threads);
    for (int ix=0 ; ix<threads ; ix+=1) {
        pix[ix] = pixb[ix] = ix * (PIXEL_COUNT / threads);
        pixe[ix] = (ix + 1) * (PIXEL_COUNT / threads);
    }

    samp.resize(threads, 0);

    auto start = hrclock::now();
    auto last_update = start;

    uint64_t ticks = S_TICKS;
    bool more = true;
    while (! exit_program && more) {
        uint8_t * pixels = (uint8_t *) surface->pixels;
        int pitch = surface->pitch;

        uint64_t when = __rdtsc() + ticks;
        vector<thread> ts;
        for (int tn=0 ; tn<threads ; tn+=1) {
            ts.push_back(thread(progressive_render_pixels, & pix[tn],
                                                           pixb[tn], pixe[tn],
                                                           when, & samp[tn],
                                                           pixels, pitch));
        }
        for (auto & t : ts) t.join();

        more = false;
        for (int ix=0 ; ix<threads ; ix+=1) if (samp[ix] < samples) more = true;

        SDL_UpdateWindowSurface(window);

        auto now = hrclock::now();
        real since_update = duration_cast<rsec>(now - last_update).count();
        if (since_update >= 60) {
            ostringstream msg;
            real total_samps = 0;
            for (auto n : samp) total_samps += n;
            real samps = total_samps / threads;
            msg << "samples: " << samps << " / " << samples << endl;
            real since_start = duration_cast<rsec>(now - start).count();
            msg << "elapsed: " << fmt_time(since_start) << endl;
            real remaining = (samples - samps) / (samps / since_start);
            msg << "estimated remaining: " << fmt_time(remaining) << endl;
            info(msg.str());
            last_update = now;
            ticks = TICKS;

            save_renderdata();
        }
    }

    auto end = hrclock::now();
    real elapsed = duration_cast<rsec>(end - start).count();
    real total_samps = 0;
    for (auto n : samp) total_samps += n;
    real samps = total_samps / threads;
    cout << "samples: " << samps << " / " << samples << endl;
    cout << "render time: " << fmt_time(elapsed) << endl;

    SDL_Event e;
    e.type = RENDER_DONE;
    SDL_PushEvent(& e);

    save_renderdata();
}
