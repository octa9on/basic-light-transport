#include <algorithm>

#include "filmic.h"

using std::max;

real toe_strength = NAN;
real toe_length = NAN;
real shoulder_strength = NAN;
real shoulder_length = NAN;
real shoulder_angle = NAN;

namespace filmic {
    pt_t p0, p1, overshoot;
    pm_t params[3];
    real w, inverse_w;
}

void set_if_nan(real & n, real v) {
    if (std::isnan(n)) n = v;
}

void setup_filmic() {
    using namespace filmic;

    /*
    set_if_nan(toe_strength, 0.5);
    set_if_nan(toe_length, 0.5);
    set_if_nan(shoulder_strength, 2.0);
    set_if_nan(shoulder_length, 0.5);
    set_if_nan(shoulder_angle, 1);

    real fake_gamma = 2.2;
    toe_length = pow(clamp01(toe_length), fake_gamma);
    toe_strength = clamp01(toe_strength);
    shoulder_angle = clamp01(shoulder_angle);
    shoulder_length = max(real(1e-5), clamp01(shoulder_length));
    shoulder_strength = max(real(0), shoulder_strength);

    p0.x = toe_length * 0.5;
    p0.y = (1 - toe_strength) * p0.x;

    real remaining_y = 1 - p0.y;
    real initial_w = p0.x + remaining_y;

    real p1_y_offset = (1 - shoulder_length) * remaining_y;
    p1.x = p0.x + p1_y_offset;
    p1.y = p0.y + p1_y_offset;

    real extra_w = exp2(shoulder_strength) - 1;
    w = initial_w + extra_w;
    inverse_w = 1 / w;

    overshoot.x = w*2 * shoulder_angle * shoulder_strength;
    overshoot.y = 0.5 * shoulder_angle * shoulder_strength;
    */

    p0 = {1/8., 1/16.};
    p1 = {3/8., 15/16.};
    w = white_pt;
    inverse_w = 1 / w;
    overshoot = {0, 0};

    // mid segment
    real dy = p1.y - p0.y;
    real dx = p1.x - p0.x;
    real m = (dx == 0) ? 0 : dy / dx;
    real b = p0.y - m * p0.x;

    auto & mid = params[1];
    mid.offset_x = -b / m;
    mid.offset_y = 0;
    mid.scale_x = 1;
    mid.scale_y = 1;
    mid.lnA = log(m);
    mid.B = 1;

    // toe segment
    auto & toe = params[0];
    toe.offset_x = 0;
    toe.offset_y = 0;
    toe.scale_x = 1;
    toe.scale_y = 1;
    toe.B = m * p0.x / p0.y;
    toe.lnA = log(p0.y) / toe.B * log(p0.x);

    // shoulder segment
    auto & sho = params[2];
    sho.offset_x = 1 + overshoot.x;
    sho.offset_y = 1 + overshoot.y;
    sho.scale_x = -1;
    sho.scale_y = -1;

    real tempx = sho.offset_x - p1.x;
    real tempy = sho.offset_y - p1.y;
    sho.B = m * tempx / tempy;
    sho.lnA = log(tempy) / sho.B * log(tempx);
}
