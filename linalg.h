#pragma once

#include <array>
#include <cmath>
#include <iostream>
#include <memory>

#include "blt.h"

using std::array;
using std::make_shared;
using std::ostream;
using std::pair;
using std::shared_ptr;

struct transform_t;

constexpr real SKOOTCH_FACTOR = 0.0001;

struct normal_t;
struct vector_t {
    real x, y, z;

    inline real sq_length() {return sq(x) + sq(y) + sq(z);}
    inline real length() {return sqrt(sq_length());}
    inline vector_t with_length(real len) {return normalize() * len;}
    inline vector_t skootch() {return with_length(SKOOTCH_FACTOR);}
    inline vector_t operator+(vector_t v) {return {x+v.x, y+v.y, z+v.z};}
    inline vector_t operator+=(vector_t v) {return {x+=v.x, y+=v.y, z+=v.z};}
    inline vector_t operator+(normal_t n);
    inline vector_t operator-() {return {-x, -y, -z};}
    inline vector_t operator-(vector_t v) {return {x-v.x, y-v.y, z-v.z};}
    inline vector_t operator*(real k) {return {x*k, y*k, z*k};}
    inline vector_t operator/(real k) {
        if (k == 0) warn("division by 0");
        return {x/k, y/k, z/k};
    }
    inline vector_t normalize() {return * this / length();}
    vector_t rotate_by(vector_t raw_axis);
    vector_t rotate(vector_t src, vector_t dst);
    vector_t from_world(transform_t & t);
    vector_t to_world(transform_t & t);
};

ostream & operator<<(ostream & out, vector_t v);

struct normal_t {
    real x, y, z;

    inline real sq_length() {return sq(x) + sq(y) + sq(z);}
    inline real length() {return sqrt(sq_length());}
    inline normal_t with_length(real len) {return normalize() * len;}
    inline normal_t skootch() {return with_length(SKOOTCH_FACTOR);}
    inline normal_t operator+(normal_t n) {return {x+n.x, y+n.y, z+n.z};}
    inline normal_t operator-() {return {-x, -y, -z};}
    inline normal_t operator-(normal_t n) {return {x-n.x, y-n.y, z-n.z};}
    inline normal_t operator*(real k) {return {x*k, y*k, z*k};}
    inline normal_t operator/(real k) {
        if (k == 0) warn("division by 0");
        return {x/k, y/k, z/k};
    }
    inline normal_t normalize() {return * this / length();}
    normal_t rotate(vector_t src, vector_t dst);
    normal_t from_world(transform_t & t);
    normal_t to_world(transform_t & t);
};

inline vector_t vector_t::operator+(normal_t n) {return {x+n.x, y+n.y, z+n.z};}

inline normal_t mknormal_t(vector_t v) {return {v.x, v.y, v.z};}
inline vector_t mkvector_t(normal_t n) {return {n.x, n.y, n.z};}

inline real dot(vector_t a, vector_t b) {return {a.x*b.x + a.y*b.y + a.z*b.z};}
inline real dot(normal_t a, vector_t b) {return {a.x*b.x + a.y*b.y + a.z*b.z};}
inline real dot(vector_t a, normal_t b) {return {a.x*b.x + a.y*b.y + a.z*b.z};}
inline real dot(normal_t a, normal_t b) {return {a.x*b.x + a.y*b.y + a.z*b.z};}
inline vector_t cross(vector_t a, vector_t b) {
    return {a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x};
}
inline normal_t cross_n(vector_t a, vector_t b) {
    return {a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x};
}

/*
inline angle(vector_t v1, vector_t v2) {
    return acos(dot(v1, v2) / (v1.length() * v2.length()));
}
*/

struct ray_t;
struct point_t {
    real x, y, z;

    inline bool operator==(point_t p) {return x==p.x && y==p.y && z==p.z;}
    inline point_t operator+(vector_t v) {return {x+v.x, y+v.y, z+v.z};}
    inline point_t operator-(vector_t v) {return {x-v.x, y-v.y, z-v.z};}
    inline point_t operator+(normal_t n) {return {x+n.x, y+n.y, z+n.z};}
    inline vector_t operator-(point_t p) {return {x-p.x, y-p.y, z-p.z};}
    inline point_t skootch(vector_t dir) {return * this + dir.skootch();}
    inline point_t skootch(normal_t dir) {return * this + dir.skootch();}
    point_t rotate(ray_t from, vector_t to);
    point_t from_world(transform_t & t);
    point_t to_world(transform_t & t);
};

ostream & operator<<(ostream & out, point_t p);

struct ray_t {
    point_t src;
    vector_t dir;

    inline ray_t skootch() {return {src + dir.skootch(), dir};}
    inline ray_t rotate(ray_t from, vector_t to) {
        point_t new_src = from.src + (src - from.src).rotate(from.dir, to);
        vector_t new_dir = dir.rotate(from.dir, to);
        return {new_src, new_dir};
    }
    ray_t from_world(transform_t & t);
    ray_t to_world(transform_t & t);
};

namespace std {
string to_string(vector_t v);
}

inline point_t project_onto(point_t point, ray_t ray) {
    if (abs(ray.dir.sq_length() - 1) > SKOOTCH_FACTOR) warn("project_onto non-normalized ray: length=" + std::to_string(ray.dir.length()));
    return ray.src + ray.dir * dot(ray.dir, point - ray.src);
}

using mat4 = array<array<real, 4>, 4>;

struct transform_t {
    mat4 mat {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
    mat4 imat {mat};

    transform_t() {}

    static transform_t identity;
    static transform_t translation(vector_t v);
    static transform_t rotation(vector_t axis, pair<real, real> cs);
    static transform_t scaling(vector_t s);

    transform_t operator*(transform_t & t2);

    vector_t apply(vector_t v);
    vector_t iapply(vector_t v);
    normal_t apply(normal_t n);
    normal_t iapply(normal_t n);
    point_t apply(point_t p);
    point_t iapply(point_t p);
};

inline void show_pose(transform_t & t) {
    for (auto & row : t.mat) {
        for (auto n : row) {
            std::cout << n << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

vector_t operator*(mat4 & m, vector_t v);
normal_t operator*(mat4 & m, normal_t n);
point_t operator*(mat4 & m, point_t p);
