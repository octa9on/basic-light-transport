#pragma once

#include <vector>

#include "blt.h"

using std::vector;

extern int MIN_BOUNCES;
extern int MAX_BOUNCES;

struct pixel_t {
    color_t sum;
    color_t ssqdiff;
    int samples;
    real errest = 1;
};
extern vector<pixel_t> pixel_samples;

extern std::string checkpoint_filename;

enum {
    BLUE=0,
    GREEN=1,
    RED=2,
    ALPHA=3,
};

extern bool naive;

void setup_renderdata(bool resume);
void render_scene(int threads, int samples);
