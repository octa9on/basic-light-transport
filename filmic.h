#pragma once

#include <cmath>

#include "base.h"

extern real toe_strength;
extern real toe_length;
extern real shoulder_strength;
extern real shoulder_length;
extern real shoulder_angle;

namespace filmic {
    struct pt_t {
        real x, y;
    };

    struct pm_t {
        real offset_x, scale_x;
        real offset_y, scale_y;
        real lnA, B;
    };

    extern pt_t p0, p1, overshoot;
    extern pm_t params[3];
    extern real w, inverse_w;
}

void setup_filmic();

inline real filmic_tone_map(real n) {
    using namespace filmic;

    n *= inverse_w;
    int ix = 1;
    if (n < p0.x) ix = 0;
    else if (p1.x < n) ix = 2;

    auto pm = params[ix];

    real x = (n - pm.offset_x) * pm.scale_x;
    real y = (x == 0) ? 0 : exp(pm.lnA + pm.B * log(x));
    y = y * pm.scale_y + pm.offset_y;

    return y;
}

constexpr bool linear = true;
constexpr real white_pt = 5;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnarrowing"
inline real pw_tone_map(real n) {
    real y;

    if constexpr (linear) {
        real m = 1 / white_pt;
        real b = 0;
        y = m * n + b;
    }
    else {
        //filmic::pt_t p0={white_pt*3/16., 3/8.},
        //                 p1={white_pt/2., 3/4.}; // big boost to dark scene
        //filmic::pt_t p0={white_pt*3/16., 3/4.},
        //                 p1={white_pt*3/8., 7/8.}; // different boost
        filmic::pt_t p0={white_pt*4/32., 3/32.},
                         p1={white_pt*18/32., 28/32.}; // actually filmic!

        real m[3] = {p0.y / p0.x,
                     (p1.y - p0.y) / (p1.x - p0.x),
                     (1 - p1.y) / (white_pt - p1.x)};
        real b[3] = {0,
                     p0.y - m[1] * p0.x,
                     p1.y - m[2] * p1.x};

        int ix = 1;
        if (n < p0.x) ix = 0;
        else if (p1.x < n) ix = 2;

        y = m[ix] * n + b[ix];
    }

    return clamp01(y);
}
#pragma GCC diagnostic pop
