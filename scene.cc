#include "camera.h"
#include "world.h"
#include "filmic.h"

//#define LIGHT_OVEN

#ifdef LIGHT_OVEN

blank sky;

emissive_lambertian halfnhalf({0.5,0.5,0.5}, {0.5,0.5,0.5});

perfect_mirror mirror;
blend s_half(0.5, & sky, & mirror);
add_emissive s_halfnhalf({0.5,0.5,0.5}, & s_half);

scene_t scene(& sky, {
    /*
    obj(new plane_s({-2,0,0}, {1,0,0}), & halfnhalf),
    obj(new plane_s({2,0,0}, {-1,0,0}), & halfnhalf),
    obj(new plane_s({0,-2,0}, {0,1,0}), & halfnhalf),
    obj(new plane_s({0,2,0}, {0,-1,0}), & halfnhalf),
    obj(new plane_s({0,0,-2}, {0,0,1}), & halfnhalf),
    obj(new plane_s({0,0,2}, {0,0,-1}), & halfnhalf),
    */
    obj(new box_s({-2,0,0}, 0.001,2,2), & halfnhalf),
    obj(new box_s({2,0,0}, 0.001,2,2), & halfnhalf),
    obj(new box_s({0,-2,0}, 2,0.001,2), & halfnhalf),
    obj(new box_s({0,2,0}, 2,0.001,2), & halfnhalf),
    obj(new box_s({0,0,-2}, 2,2,0.001), & halfnhalf),
    obj(new box_s({0,0,2}, 2,2,0.001), & halfnhalf),

    //obj(new box_s({0,0,0}, 0.5,0.5,0.5), & mirror),
    obj(new sphere_s({0,0,0}, 0.5), & halfnhalf),
});

auto * camera = new camera_t({-1.9,-1.9,-1.9}, {0,0,0});

#else

blank sky;

emissive lamp((color_t) {1,1,1} * white_pt);
emissive yellowlamp((color_t) {1,1,0} * 4/5. * white_pt);

real more = 0.8;
real less = 0.4;
real none = 0.04;

lambertian white {(color_t) {more,more,more}};
lambertian red {(color_t) {more, none, none}};
lambertian green {(color_t) {none, more, none}};
lambertian blue {(color_t) {none, none, more}};
lambertian yellow {(color_t) {more, more, none}};
lambertian violet {(color_t) {79/255.,47/255.,79/255.}};
lambertian rose {(color_t) {155/255.,39/255.,90/255.}};

lambertian palered {(color_t) {more, less, less}};
lambertian palegreen {(color_t) {less, more, less}};
lambertian paleblue {(color_t) {less, less, more}};
lambertian paleyellow {(color_t) {more, more, less}};

lambertian black {(color_t) {none,none,none}};
perfect_mirror mirror;

blend red_mirror {0.9, & red, & mirror};

blend palered_mirror {0.9, & palered, & mirror};
blend palegreen_mirror {0.9, & palegreen, & mirror};
blend paleblue_mirror {0.9, & paleblue, & mirror};
blend paleyellow_mirror {0.9, & paleyellow, & mirror};

blend white_mirror {0.9, & white, & mirror};
blend black_mirror {0.95, & black, & mirror};

u_stripe ball12 {1/3., & violet, & white};
blend ball12_mirror {0.9, & ball12, & mirror};

crosshatch xball {10, & rose, & palegreen};
blend xball_mirror {0.9, & xball, & mirror};

beachball bball({& white, & red, & blue, & yellow});
blend bball_mirror {0.9, & bball, & mirror};

blend marble {0.5, & black, & mirror};
blend wall_mirror {0.25, & black, & mirror};

texture_t earth_map("earthmap1k.jpg");
texture_t earth_specular("earthspec1k.jpg");
texture_t earth_lights("land_lights_2000-darkened.png");
lambertian_map earth_color(& earth_map);
blend earth_color_mirror(0.9, & earth_color, & mirror); //XXX wrong
blend_map earth_reflect(& earth_specular, & earth_color, & earth_color_mirror);
emissive_map earth(1/10., & earth_lights, & earth_reflect);

lambertian perfect_white {(color_t) {1,1,1}};
u_stripes redblackstripe(10, & red, & black);
blend redblackstripe_mirror(0.9, & redblackstripe, & mirror);

emissive_lambertian dimredlamp((color_t) {0.05,0.005,0.005}, (color_t) {0.15,0.005,0.005} * white_pt);
u_stripes dimredlamp_blackstripe(10, & dimredlamp, & black);

blend dimredlamp_blackstripe_mirror(0.9, & dimredlamp_blackstripe, & mirror);

texture_t great_map("I am great.png");
lambertian_map great(& great_map);
lambertian biggins_blue((color_t) {0.2, 0.5, 0.6});

real biggins_fn(real y, real r) {
    real top = sqrt(sq(2/3. * y) + pow(r, 7/5.)) - 1;
    real bot = cbrt(sq(5/4. * y) + cb(r)) - 1;
    return min(bot, max(top, - y));
}

/*
real weird_cone_fn(real y, real r) {
    real top = y + 7/4. * r - 5/4.;
    real k = sqrt(5) / 2;
    real mid = y - sq(k*r - k); // restrict to r <= 1
    real bot = sqrt(sq(2/3. * y) + pow(r, 7/5.)) - 1;
    return bot;
}
*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnarrowing"

/*
real ap = 0.1;

scene_t scene({
    obj(new box_s({-4,2,0}, 1,0.05,1), & lamp),
    obj(new box_s({-4,2.01,0}, 1.01,0.05,1.01), & black_mirror),

    obj(new plane_s({-2-4,0,0}, {1,0,0}), & white),
    obj(new plane_s({2,0,0}, {-1,0,0}), & white),
    obj(new plane_s({0,-2,0}, {0,1,0}), & white),
    obj(new plane_s({0,2,0}, {0,-1,0}), & white),
    obj(new one_sided_plane_s({0,0,-2}, {0,0,1}), & white),
    obj(new plane_s({0,0,2}, {0,0,-1}), & white),

    obj(new box_s({-2.01,1+ap/2,0}, 0.02,1-ap/2,2), & white),
    obj(new box_s({-2.01,-1-ap/2,0}, 0.02,1-ap/2,2), & white),
    obj(new box_s({-2.01,0,1+ap}, 0.02,ap,1-ap), & white),
    obj(new box_s({-2.01,0,-1-ap}, 0.02,ap,1-ap), & white),

    obj(new sphere_s({0,-2+1,0}, 1), & earth)
});

auto * camera = new camera_t({0,0,-(2+2*sqrt(3))}, {0,0,0});
*/

/*
real pupil_dz = (point_t({-1/3.,2,-7/12.}) - point_t({-1/3.,2+1/20.,-17/24.})).length();
point_t pupil1 = point_t({-1/3.,2-exp(-1)-4/5.,-7/12.}) + vector_t({0,4/20.,-6/24.}).with_length(pupil_dz);
point_t pupil2 = point_t({1/3.,2-exp(-1)-4/5.,-7/12.}) + vector_t({0,-1/10.,-1}).with_length(pupil_dz);

real ek = 2/3.;
scene_t scene(
    obj(new skysphere_s, & lamp),
    obj(new plane_s({0,0,0}, {0,1,0}), & perfect_white),

    scale({-5/2.,0,0}, {5/12.,1/2.,5/12.}, move({-5/2.,4/5.,0},
        obj(new revsolid_s(biggins_fn, sphere_s({0,0,0}, 1.5)), & violet),
        scale({-1/3.,2-exp(-1)-4/5.,-7/12.}, {12/5.*ek,2/1.*ek,12/5.*ek},
            obj(new sphere_s({-1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
            obj(new sphere_s(pupil1, 1/10.), & black_mirror)
        ),
        scale({1/3.,2-exp(-1)-4/5.,-7/12.}, {12/5.*ek,2/1.*ek,12/5.*ek},
            obj(new sphere_s({1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
            obj(new sphere_s(pupil2, 1/10.), & black_mirror)
        )
    )),

    scale({-9/6.,0,-1}, {5/12.,1/2.,5/12.}, move({-9/6.,4/5.,-1},
        obj(new revsolid_s(biggins_fn, sphere_s({0,0,0}, 1.5)), & rose),
        scale({-1/3.,2-exp(-1)-4/5.,-7/12.}, {12/5.*ek,2/1.*ek,12/5.*ek},
            obj(new sphere_s({-1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
            obj(new sphere_s(pupil1, 1/10.), & black_mirror)
        ),
        scale({1/3.,2-exp(-1)-4/5.,-7/12.}, {12/5.*ek,2/1.*ek,12/5.*ek},
            obj(new sphere_s({1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
            obj(new sphere_s(pupil2, 1/10.), & black_mirror)
        )
    )),

    move({-1/3.,4/5.,0}, rot({0,0,0}, {0,1,0}, - M_PI/8,
        obj(new revsolid_s(biggins_fn, sphere_s({0,0,0}, 1.5)), & great),
        scale({-1/3.,2-exp(-1)-4/5.,-7/12.}, 8/7.,
            obj(new sphere_s({-1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
            obj(new sphere_s(pupil1, 1/10.), & black_mirror)
        ),
        scale({1/3.,2-exp(-1)-4/5.,-7/12.}, 8/7.,
            obj(new sphere_s({1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
            obj(new sphere_s(pupil2, 1/10.), & black_mirror)
        )
    )),

    scale({2,0,0}, 8/7., move({2,4/5.,0}, rot({0,0,0}, {0,1,0}, M_PI/8,
        obj(new revsolid_s(biggins_fn, sphere_s({0,0,0}, 1.5)), & black),
        obj(new sphere_s({-1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
        obj(new sphere_s(pupil1, 1/10.), & black_mirror),
        obj(new sphere_s({1/3.,2-exp(-1)-4/5.,-7/12.}, 1/5.), & white_mirror),
        obj(new sphere_s(pupil2, 1/10.), & black_mirror)
    )))
);

auto * camera = new camera_t({-0.5,1,-6}, {0,1,0});
//auto * camera = new camera_t({-0.5,3/2.,-6}, {0,2,0});
*/

/*
real dz = 2 - (sqrt(8/3.)/2 + 1/3.);
real R = sqrt(8/3.)/2;

scene_t scene({
    obj(new box_s({0,2,0}, 1,0.05,1), & lamp),
    obj(new box_s({0,2.01,0}, 1.01,0.05,1.01), & black_mirror),

    rot({{0,0,0},{0,0,1}}, {0,1,0},
        rot({{0,0,1 -dz}, {0,1,0}}, {1/3.,-1/3.,1/2.},
            obj(new sphere_s({0,0,1 -dz}, R), & ball12_mirror)),
        rot({{sqrt(2/9.),sqrt(2/3.),-1/3. -dz},{0,0,1}}, {1,1,1},
            obj(new sphere_s({sqrt(2/9.),sqrt(2/3.),-1/3. -dz}, R),
                & xball_mirror)),
        rot({{-sqrt(8/9.),0,-1/3. -dz},{0,1,0}}, {1/5.,2/3.,1/3.},
            obj(new sphere_s({-sqrt(8/9.),0,-1/3. -dz}, R), & bball_mirror)),
        rot({{sqrt(2/9.),-sqrt(2/3.),-1/3. -dz}, {-1,0,0}}, {-1,-2/3.,0},
        rot({{sqrt(2/9.),-sqrt(2/3.),-1/3. -dz}, {0,0,1}}, {2/3.,0,1},
        rot({{sqrt(2/9.),-sqrt(2/3.),-1/3. -dz}, {0,0,-1}}, {0,1,0},
        rot((point_t) {sqrt(2/9.),-sqrt(2/3.),-1/3. -dz}, {0,1,0}, M_PI,
            obj(new sphere_s({sqrt(2/9.),-sqrt(2/3.),-1/3. -dz}, R), & earth))
        )))
    ),

    obj(new sphere_s({-1,-2+0.1,-1}, 0.1), & marble),

    obj(new plane_s({-2,0,0}, {1,0,0}), & white),
    obj(new plane_s({2,0,0}, {-1,0,0}), & wall_mirror),
    obj(new plane_s({0,-2,0}, {0,1,0}), & white),
    obj(new plane_s({0,2,0}, {0,-1,0}), & white),
    obj(new one_sided_plane_s({0,0,-2}, {0,0,1}), & white),
    obj(new plane_s({0,0,2}, {0,0,-1}), & white),
});
auto * camera = new camera_t({0,0,-(2+2*sqrt(3))}, {0,0,0});
*/

real dz = 2 - (sqrt(8/3.)/2 + 1/3.);
real R = sqrt(8/3.)/2;

scene_t scene(
    obj(new box_s({0,2,0}, 1,0.05,1), & lamp),
    obj(new box_s({0,2.01,0}, 1.01,0.05,1.01), & black_mirror),

    rot((point_t) {0,0,0}, {0,1,0}, - M_PI/10, rot({{0,0,0},{0,0,1}}, {0,1,0},
        obj(new sphere_s({0,0,1 -dz}, R), & palered),
        obj(new sphere_s({sqrt(2/9.),sqrt(2/3.),-1/3. -dz}, R), & paleblue),
        obj(new sphere_s({-sqrt(8/9.),0,-1/3. -dz}, R), & palegreen),
        obj(new sphere_s({sqrt(2/9.),-sqrt(2/3.),-1/3. -dz}, R), & paleyellow)
    )),

    obj(new plane_s({-2,0,0}, {1,0,0}), & white),
    obj(new plane_s({2,0,0}, {-1,0,0}), & white),
    obj(new plane_s({0,-2,0}, {0,1,0}), & white),
    obj(new plane_s({0,2,0}, {0,-1,0}), & white),
    obj(new one_sided_plane_s({0,0,-2}, {0,0,1}), & white),
    obj(new plane_s({0,0,2}, {0,0,-1}), & white)
);

auto * camera = new camera_t({0,0,-(2+2*sqrt(3))}, {0,0,0});

/*
blank sky;

emissive lamp((color_t) {1,1,1} * white_pt);
emissive bluelamp((color_t) {0.7,0.7,1} * white_pt);
emissive yellowlamp((color_t) {1,1,0.7} * white_pt);
emissive_lambertian dimredlamp((color_t) {0.04,0.04,0.04}, (color_t) {0.05,0.005,0.005} * white_pt);

perfect_mirror mirror;

lambertian black {(color_t) {0,0,0}};
lambertian grey {(color_t) {0.5,0.5,0.5}};
lambertian white {(color_t) {1,1,1}};
lambertian red {(color_t) {1,0,0}};
lambertian dullred {(color_t) {0.6,0.4,0.4}};
lambertian green {(color_t) {0,1,0}};
lambertian dullgreen {(color_t) {0.4,0.6,0.4}};
lambertian cyan {(color_t) {0,1,1}};
lambertian dullcyan {(color_t) {0.4,0.6,0.6}};
lambertian purple {(color_t) {1,0,1}};
lambertian yellow {(color_t) {1,1,0}};

auto [bx,by,bz] = (real[]) {-1,1.5,0};
auto [yx,yy,yz] = (real[]) {1,1.5,1};

scene_t scene(& sky,
    {obj(new sphere_s({bx,by,bz}, 0.25), & bluelamp),
     obj(new sphere_s({yx,yy,yz}, 0.25), & yellowlamp),
     //obj(new box_s({-0.2,0.1-0.25,-0.9}, 0.1,0.1,0.1), & lamp),
     obj(new triangle_s({1,0,0}, {0,1,0}, {0,0,1}), & dullcyan),
     obj(new box_s({bx, (by-0.25)/2, bz}, 0.01, (by+0.25)/2, 0.01), & grey),
     obj(new box_s({yx, (yy-0.25)/2, yz}, 0.01, (yy+0.25)/2, 0.01), & grey),
     obj(new sphere_s({0,0,1}, 0.5), & mirror),
     obj(new plane_s({0,-0.25,0}, {0,1,0}), & dullgreen),
     //obj(new box_s({-1.5,1/3.-0.25,2.25}, 0.333,0.333,0.333), & yellow),
     //obj(new box_s({-1.5,0.1+2/3.-0.25,2.25}, 0.1,0.1,0.1), & lamp),
     obj(new box_s({-0.2,1/3.-0.25,-0.9}, 0.05,0.333,0.05), & yellow),
     obj(new box_s({-0.2,0.1+2/3.-0.25,-0.9}, 0.1,0.1,0.1), & dimredlamp),
});

auto * camera = new camera_t({1,3/3.,-3}, {0,0.5,0});
*/
#pragma GCC diagnostic pop

#endif
